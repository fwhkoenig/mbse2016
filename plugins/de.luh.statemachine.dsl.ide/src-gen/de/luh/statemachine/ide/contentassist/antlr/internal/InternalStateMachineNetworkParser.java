package de.luh.statemachine.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import de.luh.statemachine.services.StateMachineNetworkGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalStateMachineNetworkParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'asynch'", "'receive'", "'Network'", "'{'", "'channel declarations:'", "'}'", "'StateMachine'", "'initial state:'", "'states:'", "'transitions:'", "'->'", "'synch'", "'send'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalStateMachineNetworkParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalStateMachineNetworkParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalStateMachineNetworkParser.tokenNames; }
    public String getGrammarFileName() { return "InternalStateMachineNetwork.g"; }


    	private StateMachineNetworkGrammarAccess grammarAccess;

    	public void setGrammarAccess(StateMachineNetworkGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleNetwork"
    // InternalStateMachineNetwork.g:53:1: entryRuleNetwork : ruleNetwork EOF ;
    public final void entryRuleNetwork() throws RecognitionException {
        try {
            // InternalStateMachineNetwork.g:54:1: ( ruleNetwork EOF )
            // InternalStateMachineNetwork.g:55:1: ruleNetwork EOF
            {
             before(grammarAccess.getNetworkRule()); 
            pushFollow(FOLLOW_1);
            ruleNetwork();

            state._fsp--;

             after(grammarAccess.getNetworkRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNetwork"


    // $ANTLR start "ruleNetwork"
    // InternalStateMachineNetwork.g:62:1: ruleNetwork : ( ( rule__Network__Group__0 ) ) ;
    public final void ruleNetwork() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:66:2: ( ( ( rule__Network__Group__0 ) ) )
            // InternalStateMachineNetwork.g:67:2: ( ( rule__Network__Group__0 ) )
            {
            // InternalStateMachineNetwork.g:67:2: ( ( rule__Network__Group__0 ) )
            // InternalStateMachineNetwork.g:68:3: ( rule__Network__Group__0 )
            {
             before(grammarAccess.getNetworkAccess().getGroup()); 
            // InternalStateMachineNetwork.g:69:3: ( rule__Network__Group__0 )
            // InternalStateMachineNetwork.g:69:4: rule__Network__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Network__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNetworkAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNetwork"


    // $ANTLR start "entryRuleEString"
    // InternalStateMachineNetwork.g:78:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalStateMachineNetwork.g:79:1: ( ruleEString EOF )
            // InternalStateMachineNetwork.g:80:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalStateMachineNetwork.g:87:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:91:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalStateMachineNetwork.g:92:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalStateMachineNetwork.g:92:2: ( ( rule__EString__Alternatives ) )
            // InternalStateMachineNetwork.g:93:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalStateMachineNetwork.g:94:3: ( rule__EString__Alternatives )
            // InternalStateMachineNetwork.g:94:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleStateMachine"
    // InternalStateMachineNetwork.g:103:1: entryRuleStateMachine : ruleStateMachine EOF ;
    public final void entryRuleStateMachine() throws RecognitionException {
        try {
            // InternalStateMachineNetwork.g:104:1: ( ruleStateMachine EOF )
            // InternalStateMachineNetwork.g:105:1: ruleStateMachine EOF
            {
             before(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            ruleStateMachine();

            state._fsp--;

             after(grammarAccess.getStateMachineRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalStateMachineNetwork.g:112:1: ruleStateMachine : ( ( rule__StateMachine__Group__0 ) ) ;
    public final void ruleStateMachine() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:116:2: ( ( ( rule__StateMachine__Group__0 ) ) )
            // InternalStateMachineNetwork.g:117:2: ( ( rule__StateMachine__Group__0 ) )
            {
            // InternalStateMachineNetwork.g:117:2: ( ( rule__StateMachine__Group__0 ) )
            // InternalStateMachineNetwork.g:118:3: ( rule__StateMachine__Group__0 )
            {
             before(grammarAccess.getStateMachineAccess().getGroup()); 
            // InternalStateMachineNetwork.g:119:3: ( rule__StateMachine__Group__0 )
            // InternalStateMachineNetwork.g:119:4: rule__StateMachine__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleChannel"
    // InternalStateMachineNetwork.g:128:1: entryRuleChannel : ruleChannel EOF ;
    public final void entryRuleChannel() throws RecognitionException {
        try {
            // InternalStateMachineNetwork.g:129:1: ( ruleChannel EOF )
            // InternalStateMachineNetwork.g:130:1: ruleChannel EOF
            {
             before(grammarAccess.getChannelRule()); 
            pushFollow(FOLLOW_1);
            ruleChannel();

            state._fsp--;

             after(grammarAccess.getChannelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleChannel"


    // $ANTLR start "ruleChannel"
    // InternalStateMachineNetwork.g:137:1: ruleChannel : ( ( rule__Channel__Group__0 ) ) ;
    public final void ruleChannel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:141:2: ( ( ( rule__Channel__Group__0 ) ) )
            // InternalStateMachineNetwork.g:142:2: ( ( rule__Channel__Group__0 ) )
            {
            // InternalStateMachineNetwork.g:142:2: ( ( rule__Channel__Group__0 ) )
            // InternalStateMachineNetwork.g:143:3: ( rule__Channel__Group__0 )
            {
             before(grammarAccess.getChannelAccess().getGroup()); 
            // InternalStateMachineNetwork.g:144:3: ( rule__Channel__Group__0 )
            // InternalStateMachineNetwork.g:144:4: rule__Channel__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Channel__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getChannelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleChannel"


    // $ANTLR start "entryRuleState"
    // InternalStateMachineNetwork.g:153:1: entryRuleState : ruleState EOF ;
    public final void entryRuleState() throws RecognitionException {
        try {
            // InternalStateMachineNetwork.g:154:1: ( ruleState EOF )
            // InternalStateMachineNetwork.g:155:1: ruleState EOF
            {
             before(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            ruleState();

            state._fsp--;

             after(grammarAccess.getStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalStateMachineNetwork.g:162:1: ruleState : ( ( rule__State__Group__0 ) ) ;
    public final void ruleState() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:166:2: ( ( ( rule__State__Group__0 ) ) )
            // InternalStateMachineNetwork.g:167:2: ( ( rule__State__Group__0 ) )
            {
            // InternalStateMachineNetwork.g:167:2: ( ( rule__State__Group__0 ) )
            // InternalStateMachineNetwork.g:168:3: ( rule__State__Group__0 )
            {
             before(grammarAccess.getStateAccess().getGroup()); 
            // InternalStateMachineNetwork.g:169:3: ( rule__State__Group__0 )
            // InternalStateMachineNetwork.g:169:4: rule__State__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__State__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalStateMachineNetwork.g:178:1: entryRuleTransition : ruleTransition EOF ;
    public final void entryRuleTransition() throws RecognitionException {
        try {
            // InternalStateMachineNetwork.g:179:1: ( ruleTransition EOF )
            // InternalStateMachineNetwork.g:180:1: ruleTransition EOF
            {
             before(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getTransitionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalStateMachineNetwork.g:187:1: ruleTransition : ( ( rule__Transition__Group__0 ) ) ;
    public final void ruleTransition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:191:2: ( ( ( rule__Transition__Group__0 ) ) )
            // InternalStateMachineNetwork.g:192:2: ( ( rule__Transition__Group__0 ) )
            {
            // InternalStateMachineNetwork.g:192:2: ( ( rule__Transition__Group__0 ) )
            // InternalStateMachineNetwork.g:193:3: ( rule__Transition__Group__0 )
            {
             before(grammarAccess.getTransitionAccess().getGroup()); 
            // InternalStateMachineNetwork.g:194:3: ( rule__Transition__Group__0 )
            // InternalStateMachineNetwork.g:194:4: rule__Transition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalStateMachineNetwork.g:202:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:206:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_STRING) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_ID) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalStateMachineNetwork.g:207:2: ( RULE_STRING )
                    {
                    // InternalStateMachineNetwork.g:207:2: ( RULE_STRING )
                    // InternalStateMachineNetwork.g:208:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStateMachineNetwork.g:213:2: ( RULE_ID )
                    {
                    // InternalStateMachineNetwork.g:213:2: ( RULE_ID )
                    // InternalStateMachineNetwork.g:214:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__Channel__Alternatives_1"
    // InternalStateMachineNetwork.g:223:1: rule__Channel__Alternatives_1 : ( ( ( rule__Channel__SynchronousAssignment_1_0 ) ) | ( 'asynch' ) );
    public final void rule__Channel__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:227:1: ( ( ( rule__Channel__SynchronousAssignment_1_0 ) ) | ( 'asynch' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==22) ) {
                alt2=1;
            }
            else if ( (LA2_0==11) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalStateMachineNetwork.g:228:2: ( ( rule__Channel__SynchronousAssignment_1_0 ) )
                    {
                    // InternalStateMachineNetwork.g:228:2: ( ( rule__Channel__SynchronousAssignment_1_0 ) )
                    // InternalStateMachineNetwork.g:229:3: ( rule__Channel__SynchronousAssignment_1_0 )
                    {
                     before(grammarAccess.getChannelAccess().getSynchronousAssignment_1_0()); 
                    // InternalStateMachineNetwork.g:230:3: ( rule__Channel__SynchronousAssignment_1_0 )
                    // InternalStateMachineNetwork.g:230:4: rule__Channel__SynchronousAssignment_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Channel__SynchronousAssignment_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getChannelAccess().getSynchronousAssignment_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStateMachineNetwork.g:234:2: ( 'asynch' )
                    {
                    // InternalStateMachineNetwork.g:234:2: ( 'asynch' )
                    // InternalStateMachineNetwork.g:235:3: 'asynch'
                    {
                     before(grammarAccess.getChannelAccess().getAsynchKeyword_1_1()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getChannelAccess().getAsynchKeyword_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Channel__Alternatives_1"


    // $ANTLR start "rule__Transition__Alternatives_4"
    // InternalStateMachineNetwork.g:244:1: rule__Transition__Alternatives_4 : ( ( ( rule__Transition__SendingAssignment_4_0 ) ) | ( 'receive' ) );
    public final void rule__Transition__Alternatives_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:248:1: ( ( ( rule__Transition__SendingAssignment_4_0 ) ) | ( 'receive' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==23) ) {
                alt3=1;
            }
            else if ( (LA3_0==12) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalStateMachineNetwork.g:249:2: ( ( rule__Transition__SendingAssignment_4_0 ) )
                    {
                    // InternalStateMachineNetwork.g:249:2: ( ( rule__Transition__SendingAssignment_4_0 ) )
                    // InternalStateMachineNetwork.g:250:3: ( rule__Transition__SendingAssignment_4_0 )
                    {
                     before(grammarAccess.getTransitionAccess().getSendingAssignment_4_0()); 
                    // InternalStateMachineNetwork.g:251:3: ( rule__Transition__SendingAssignment_4_0 )
                    // InternalStateMachineNetwork.g:251:4: rule__Transition__SendingAssignment_4_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Transition__SendingAssignment_4_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getTransitionAccess().getSendingAssignment_4_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStateMachineNetwork.g:255:2: ( 'receive' )
                    {
                    // InternalStateMachineNetwork.g:255:2: ( 'receive' )
                    // InternalStateMachineNetwork.g:256:3: 'receive'
                    {
                     before(grammarAccess.getTransitionAccess().getReceiveKeyword_4_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getTransitionAccess().getReceiveKeyword_4_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Alternatives_4"


    // $ANTLR start "rule__Network__Group__0"
    // InternalStateMachineNetwork.g:265:1: rule__Network__Group__0 : rule__Network__Group__0__Impl rule__Network__Group__1 ;
    public final void rule__Network__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:269:1: ( rule__Network__Group__0__Impl rule__Network__Group__1 )
            // InternalStateMachineNetwork.g:270:2: rule__Network__Group__0__Impl rule__Network__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Network__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Network__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__0"


    // $ANTLR start "rule__Network__Group__0__Impl"
    // InternalStateMachineNetwork.g:277:1: rule__Network__Group__0__Impl : ( () ) ;
    public final void rule__Network__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:281:1: ( ( () ) )
            // InternalStateMachineNetwork.g:282:1: ( () )
            {
            // InternalStateMachineNetwork.g:282:1: ( () )
            // InternalStateMachineNetwork.g:283:2: ()
            {
             before(grammarAccess.getNetworkAccess().getNetworkAction_0()); 
            // InternalStateMachineNetwork.g:284:2: ()
            // InternalStateMachineNetwork.g:284:3: 
            {
            }

             after(grammarAccess.getNetworkAccess().getNetworkAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__0__Impl"


    // $ANTLR start "rule__Network__Group__1"
    // InternalStateMachineNetwork.g:292:1: rule__Network__Group__1 : rule__Network__Group__1__Impl rule__Network__Group__2 ;
    public final void rule__Network__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:296:1: ( rule__Network__Group__1__Impl rule__Network__Group__2 )
            // InternalStateMachineNetwork.g:297:2: rule__Network__Group__1__Impl rule__Network__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Network__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Network__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__1"


    // $ANTLR start "rule__Network__Group__1__Impl"
    // InternalStateMachineNetwork.g:304:1: rule__Network__Group__1__Impl : ( 'Network' ) ;
    public final void rule__Network__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:308:1: ( ( 'Network' ) )
            // InternalStateMachineNetwork.g:309:1: ( 'Network' )
            {
            // InternalStateMachineNetwork.g:309:1: ( 'Network' )
            // InternalStateMachineNetwork.g:310:2: 'Network'
            {
             before(grammarAccess.getNetworkAccess().getNetworkKeyword_1()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getNetworkAccess().getNetworkKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__1__Impl"


    // $ANTLR start "rule__Network__Group__2"
    // InternalStateMachineNetwork.g:319:1: rule__Network__Group__2 : rule__Network__Group__2__Impl rule__Network__Group__3 ;
    public final void rule__Network__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:323:1: ( rule__Network__Group__2__Impl rule__Network__Group__3 )
            // InternalStateMachineNetwork.g:324:2: rule__Network__Group__2__Impl rule__Network__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Network__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Network__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__2"


    // $ANTLR start "rule__Network__Group__2__Impl"
    // InternalStateMachineNetwork.g:331:1: rule__Network__Group__2__Impl : ( ( rule__Network__NameAssignment_2 ) ) ;
    public final void rule__Network__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:335:1: ( ( ( rule__Network__NameAssignment_2 ) ) )
            // InternalStateMachineNetwork.g:336:1: ( ( rule__Network__NameAssignment_2 ) )
            {
            // InternalStateMachineNetwork.g:336:1: ( ( rule__Network__NameAssignment_2 ) )
            // InternalStateMachineNetwork.g:337:2: ( rule__Network__NameAssignment_2 )
            {
             before(grammarAccess.getNetworkAccess().getNameAssignment_2()); 
            // InternalStateMachineNetwork.g:338:2: ( rule__Network__NameAssignment_2 )
            // InternalStateMachineNetwork.g:338:3: rule__Network__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Network__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getNetworkAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__2__Impl"


    // $ANTLR start "rule__Network__Group__3"
    // InternalStateMachineNetwork.g:346:1: rule__Network__Group__3 : rule__Network__Group__3__Impl rule__Network__Group__4 ;
    public final void rule__Network__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:350:1: ( rule__Network__Group__3__Impl rule__Network__Group__4 )
            // InternalStateMachineNetwork.g:351:2: rule__Network__Group__3__Impl rule__Network__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Network__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Network__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__3"


    // $ANTLR start "rule__Network__Group__3__Impl"
    // InternalStateMachineNetwork.g:358:1: rule__Network__Group__3__Impl : ( '{' ) ;
    public final void rule__Network__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:362:1: ( ( '{' ) )
            // InternalStateMachineNetwork.g:363:1: ( '{' )
            {
            // InternalStateMachineNetwork.g:363:1: ( '{' )
            // InternalStateMachineNetwork.g:364:2: '{'
            {
             before(grammarAccess.getNetworkAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getNetworkAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__3__Impl"


    // $ANTLR start "rule__Network__Group__4"
    // InternalStateMachineNetwork.g:373:1: rule__Network__Group__4 : rule__Network__Group__4__Impl rule__Network__Group__5 ;
    public final void rule__Network__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:377:1: ( rule__Network__Group__4__Impl rule__Network__Group__5 )
            // InternalStateMachineNetwork.g:378:2: rule__Network__Group__4__Impl rule__Network__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Network__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Network__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__4"


    // $ANTLR start "rule__Network__Group__4__Impl"
    // InternalStateMachineNetwork.g:385:1: rule__Network__Group__4__Impl : ( 'channel declarations:' ) ;
    public final void rule__Network__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:389:1: ( ( 'channel declarations:' ) )
            // InternalStateMachineNetwork.g:390:1: ( 'channel declarations:' )
            {
            // InternalStateMachineNetwork.g:390:1: ( 'channel declarations:' )
            // InternalStateMachineNetwork.g:391:2: 'channel declarations:'
            {
             before(grammarAccess.getNetworkAccess().getChannelDeclarationsKeyword_4()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getNetworkAccess().getChannelDeclarationsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__4__Impl"


    // $ANTLR start "rule__Network__Group__5"
    // InternalStateMachineNetwork.g:400:1: rule__Network__Group__5 : rule__Network__Group__5__Impl rule__Network__Group__6 ;
    public final void rule__Network__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:404:1: ( rule__Network__Group__5__Impl rule__Network__Group__6 )
            // InternalStateMachineNetwork.g:405:2: rule__Network__Group__5__Impl rule__Network__Group__6
            {
            pushFollow(FOLLOW_7);
            rule__Network__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Network__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__5"


    // $ANTLR start "rule__Network__Group__5__Impl"
    // InternalStateMachineNetwork.g:412:1: rule__Network__Group__5__Impl : ( ( rule__Network__ChannelsAssignment_5 )* ) ;
    public final void rule__Network__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:416:1: ( ( ( rule__Network__ChannelsAssignment_5 )* ) )
            // InternalStateMachineNetwork.g:417:1: ( ( rule__Network__ChannelsAssignment_5 )* )
            {
            // InternalStateMachineNetwork.g:417:1: ( ( rule__Network__ChannelsAssignment_5 )* )
            // InternalStateMachineNetwork.g:418:2: ( rule__Network__ChannelsAssignment_5 )*
            {
             before(grammarAccess.getNetworkAccess().getChannelsAssignment_5()); 
            // InternalStateMachineNetwork.g:419:2: ( rule__Network__ChannelsAssignment_5 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==11||LA4_0==22) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalStateMachineNetwork.g:419:3: rule__Network__ChannelsAssignment_5
            	    {
            	    pushFollow(FOLLOW_8);
            	    rule__Network__ChannelsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getNetworkAccess().getChannelsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__5__Impl"


    // $ANTLR start "rule__Network__Group__6"
    // InternalStateMachineNetwork.g:427:1: rule__Network__Group__6 : rule__Network__Group__6__Impl rule__Network__Group__7 ;
    public final void rule__Network__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:431:1: ( rule__Network__Group__6__Impl rule__Network__Group__7 )
            // InternalStateMachineNetwork.g:432:2: rule__Network__Group__6__Impl rule__Network__Group__7
            {
            pushFollow(FOLLOW_7);
            rule__Network__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Network__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__6"


    // $ANTLR start "rule__Network__Group__6__Impl"
    // InternalStateMachineNetwork.g:439:1: rule__Network__Group__6__Impl : ( ( rule__Network__StateMachinesAssignment_6 )* ) ;
    public final void rule__Network__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:443:1: ( ( ( rule__Network__StateMachinesAssignment_6 )* ) )
            // InternalStateMachineNetwork.g:444:1: ( ( rule__Network__StateMachinesAssignment_6 )* )
            {
            // InternalStateMachineNetwork.g:444:1: ( ( rule__Network__StateMachinesAssignment_6 )* )
            // InternalStateMachineNetwork.g:445:2: ( rule__Network__StateMachinesAssignment_6 )*
            {
             before(grammarAccess.getNetworkAccess().getStateMachinesAssignment_6()); 
            // InternalStateMachineNetwork.g:446:2: ( rule__Network__StateMachinesAssignment_6 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==17) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalStateMachineNetwork.g:446:3: rule__Network__StateMachinesAssignment_6
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Network__StateMachinesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getNetworkAccess().getStateMachinesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__6__Impl"


    // $ANTLR start "rule__Network__Group__7"
    // InternalStateMachineNetwork.g:454:1: rule__Network__Group__7 : rule__Network__Group__7__Impl ;
    public final void rule__Network__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:458:1: ( rule__Network__Group__7__Impl )
            // InternalStateMachineNetwork.g:459:2: rule__Network__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Network__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__7"


    // $ANTLR start "rule__Network__Group__7__Impl"
    // InternalStateMachineNetwork.g:465:1: rule__Network__Group__7__Impl : ( '}' ) ;
    public final void rule__Network__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:469:1: ( ( '}' ) )
            // InternalStateMachineNetwork.g:470:1: ( '}' )
            {
            // InternalStateMachineNetwork.g:470:1: ( '}' )
            // InternalStateMachineNetwork.g:471:2: '}'
            {
             before(grammarAccess.getNetworkAccess().getRightCurlyBracketKeyword_7()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getNetworkAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__Group__7__Impl"


    // $ANTLR start "rule__StateMachine__Group__0"
    // InternalStateMachineNetwork.g:481:1: rule__StateMachine__Group__0 : rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1 ;
    public final void rule__StateMachine__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:485:1: ( rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1 )
            // InternalStateMachineNetwork.g:486:2: rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__StateMachine__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__0"


    // $ANTLR start "rule__StateMachine__Group__0__Impl"
    // InternalStateMachineNetwork.g:493:1: rule__StateMachine__Group__0__Impl : ( () ) ;
    public final void rule__StateMachine__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:497:1: ( ( () ) )
            // InternalStateMachineNetwork.g:498:1: ( () )
            {
            // InternalStateMachineNetwork.g:498:1: ( () )
            // InternalStateMachineNetwork.g:499:2: ()
            {
             before(grammarAccess.getStateMachineAccess().getStateMachineAction_0()); 
            // InternalStateMachineNetwork.g:500:2: ()
            // InternalStateMachineNetwork.g:500:3: 
            {
            }

             after(grammarAccess.getStateMachineAccess().getStateMachineAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__0__Impl"


    // $ANTLR start "rule__StateMachine__Group__1"
    // InternalStateMachineNetwork.g:508:1: rule__StateMachine__Group__1 : rule__StateMachine__Group__1__Impl rule__StateMachine__Group__2 ;
    public final void rule__StateMachine__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:512:1: ( rule__StateMachine__Group__1__Impl rule__StateMachine__Group__2 )
            // InternalStateMachineNetwork.g:513:2: rule__StateMachine__Group__1__Impl rule__StateMachine__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__StateMachine__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__1"


    // $ANTLR start "rule__StateMachine__Group__1__Impl"
    // InternalStateMachineNetwork.g:520:1: rule__StateMachine__Group__1__Impl : ( 'StateMachine' ) ;
    public final void rule__StateMachine__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:524:1: ( ( 'StateMachine' ) )
            // InternalStateMachineNetwork.g:525:1: ( 'StateMachine' )
            {
            // InternalStateMachineNetwork.g:525:1: ( 'StateMachine' )
            // InternalStateMachineNetwork.g:526:2: 'StateMachine'
            {
             before(grammarAccess.getStateMachineAccess().getStateMachineKeyword_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getStateMachineKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__1__Impl"


    // $ANTLR start "rule__StateMachine__Group__2"
    // InternalStateMachineNetwork.g:535:1: rule__StateMachine__Group__2 : rule__StateMachine__Group__2__Impl rule__StateMachine__Group__3 ;
    public final void rule__StateMachine__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:539:1: ( rule__StateMachine__Group__2__Impl rule__StateMachine__Group__3 )
            // InternalStateMachineNetwork.g:540:2: rule__StateMachine__Group__2__Impl rule__StateMachine__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__StateMachine__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__2"


    // $ANTLR start "rule__StateMachine__Group__2__Impl"
    // InternalStateMachineNetwork.g:547:1: rule__StateMachine__Group__2__Impl : ( ( rule__StateMachine__NameAssignment_2 ) ) ;
    public final void rule__StateMachine__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:551:1: ( ( ( rule__StateMachine__NameAssignment_2 ) ) )
            // InternalStateMachineNetwork.g:552:1: ( ( rule__StateMachine__NameAssignment_2 ) )
            {
            // InternalStateMachineNetwork.g:552:1: ( ( rule__StateMachine__NameAssignment_2 ) )
            // InternalStateMachineNetwork.g:553:2: ( rule__StateMachine__NameAssignment_2 )
            {
             before(grammarAccess.getStateMachineAccess().getNameAssignment_2()); 
            // InternalStateMachineNetwork.g:554:2: ( rule__StateMachine__NameAssignment_2 )
            // InternalStateMachineNetwork.g:554:3: rule__StateMachine__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__2__Impl"


    // $ANTLR start "rule__StateMachine__Group__3"
    // InternalStateMachineNetwork.g:562:1: rule__StateMachine__Group__3 : rule__StateMachine__Group__3__Impl rule__StateMachine__Group__4 ;
    public final void rule__StateMachine__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:566:1: ( rule__StateMachine__Group__3__Impl rule__StateMachine__Group__4 )
            // InternalStateMachineNetwork.g:567:2: rule__StateMachine__Group__3__Impl rule__StateMachine__Group__4
            {
            pushFollow(FOLLOW_11);
            rule__StateMachine__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__3"


    // $ANTLR start "rule__StateMachine__Group__3__Impl"
    // InternalStateMachineNetwork.g:574:1: rule__StateMachine__Group__3__Impl : ( '{' ) ;
    public final void rule__StateMachine__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:578:1: ( ( '{' ) )
            // InternalStateMachineNetwork.g:579:1: ( '{' )
            {
            // InternalStateMachineNetwork.g:579:1: ( '{' )
            // InternalStateMachineNetwork.g:580:2: '{'
            {
             before(grammarAccess.getStateMachineAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__3__Impl"


    // $ANTLR start "rule__StateMachine__Group__4"
    // InternalStateMachineNetwork.g:589:1: rule__StateMachine__Group__4 : rule__StateMachine__Group__4__Impl rule__StateMachine__Group__5 ;
    public final void rule__StateMachine__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:593:1: ( rule__StateMachine__Group__4__Impl rule__StateMachine__Group__5 )
            // InternalStateMachineNetwork.g:594:2: rule__StateMachine__Group__4__Impl rule__StateMachine__Group__5
            {
            pushFollow(FOLLOW_11);
            rule__StateMachine__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__4"


    // $ANTLR start "rule__StateMachine__Group__4__Impl"
    // InternalStateMachineNetwork.g:601:1: rule__StateMachine__Group__4__Impl : ( ( rule__StateMachine__Group_4__0 )? ) ;
    public final void rule__StateMachine__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:605:1: ( ( ( rule__StateMachine__Group_4__0 )? ) )
            // InternalStateMachineNetwork.g:606:1: ( ( rule__StateMachine__Group_4__0 )? )
            {
            // InternalStateMachineNetwork.g:606:1: ( ( rule__StateMachine__Group_4__0 )? )
            // InternalStateMachineNetwork.g:607:2: ( rule__StateMachine__Group_4__0 )?
            {
             before(grammarAccess.getStateMachineAccess().getGroup_4()); 
            // InternalStateMachineNetwork.g:608:2: ( rule__StateMachine__Group_4__0 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==18) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalStateMachineNetwork.g:608:3: rule__StateMachine__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__StateMachine__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStateMachineAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__4__Impl"


    // $ANTLR start "rule__StateMachine__Group__5"
    // InternalStateMachineNetwork.g:616:1: rule__StateMachine__Group__5 : rule__StateMachine__Group__5__Impl ;
    public final void rule__StateMachine__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:620:1: ( rule__StateMachine__Group__5__Impl )
            // InternalStateMachineNetwork.g:621:2: rule__StateMachine__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__5"


    // $ANTLR start "rule__StateMachine__Group__5__Impl"
    // InternalStateMachineNetwork.g:627:1: rule__StateMachine__Group__5__Impl : ( '}' ) ;
    public final void rule__StateMachine__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:631:1: ( ( '}' ) )
            // InternalStateMachineNetwork.g:632:1: ( '}' )
            {
            // InternalStateMachineNetwork.g:632:1: ( '}' )
            // InternalStateMachineNetwork.g:633:2: '}'
            {
             before(grammarAccess.getStateMachineAccess().getRightCurlyBracketKeyword_5()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__5__Impl"


    // $ANTLR start "rule__StateMachine__Group_4__0"
    // InternalStateMachineNetwork.g:643:1: rule__StateMachine__Group_4__0 : rule__StateMachine__Group_4__0__Impl rule__StateMachine__Group_4__1 ;
    public final void rule__StateMachine__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:647:1: ( rule__StateMachine__Group_4__0__Impl rule__StateMachine__Group_4__1 )
            // InternalStateMachineNetwork.g:648:2: rule__StateMachine__Group_4__0__Impl rule__StateMachine__Group_4__1
            {
            pushFollow(FOLLOW_4);
            rule__StateMachine__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4__0"


    // $ANTLR start "rule__StateMachine__Group_4__0__Impl"
    // InternalStateMachineNetwork.g:655:1: rule__StateMachine__Group_4__0__Impl : ( 'initial state:' ) ;
    public final void rule__StateMachine__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:659:1: ( ( 'initial state:' ) )
            // InternalStateMachineNetwork.g:660:1: ( 'initial state:' )
            {
            // InternalStateMachineNetwork.g:660:1: ( 'initial state:' )
            // InternalStateMachineNetwork.g:661:2: 'initial state:'
            {
             before(grammarAccess.getStateMachineAccess().getInitialStateKeyword_4_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getInitialStateKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4__0__Impl"


    // $ANTLR start "rule__StateMachine__Group_4__1"
    // InternalStateMachineNetwork.g:670:1: rule__StateMachine__Group_4__1 : rule__StateMachine__Group_4__1__Impl rule__StateMachine__Group_4__2 ;
    public final void rule__StateMachine__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:674:1: ( rule__StateMachine__Group_4__1__Impl rule__StateMachine__Group_4__2 )
            // InternalStateMachineNetwork.g:675:2: rule__StateMachine__Group_4__1__Impl rule__StateMachine__Group_4__2
            {
            pushFollow(FOLLOW_12);
            rule__StateMachine__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4__1"


    // $ANTLR start "rule__StateMachine__Group_4__1__Impl"
    // InternalStateMachineNetwork.g:682:1: rule__StateMachine__Group_4__1__Impl : ( ( rule__StateMachine__StatesAssignment_4_1 ) ) ;
    public final void rule__StateMachine__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:686:1: ( ( ( rule__StateMachine__StatesAssignment_4_1 ) ) )
            // InternalStateMachineNetwork.g:687:1: ( ( rule__StateMachine__StatesAssignment_4_1 ) )
            {
            // InternalStateMachineNetwork.g:687:1: ( ( rule__StateMachine__StatesAssignment_4_1 ) )
            // InternalStateMachineNetwork.g:688:2: ( rule__StateMachine__StatesAssignment_4_1 )
            {
             before(grammarAccess.getStateMachineAccess().getStatesAssignment_4_1()); 
            // InternalStateMachineNetwork.g:689:2: ( rule__StateMachine__StatesAssignment_4_1 )
            // InternalStateMachineNetwork.g:689:3: rule__StateMachine__StatesAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__StatesAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getStatesAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4__1__Impl"


    // $ANTLR start "rule__StateMachine__Group_4__2"
    // InternalStateMachineNetwork.g:697:1: rule__StateMachine__Group_4__2 : rule__StateMachine__Group_4__2__Impl rule__StateMachine__Group_4__3 ;
    public final void rule__StateMachine__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:701:1: ( rule__StateMachine__Group_4__2__Impl rule__StateMachine__Group_4__3 )
            // InternalStateMachineNetwork.g:702:2: rule__StateMachine__Group_4__2__Impl rule__StateMachine__Group_4__3
            {
            pushFollow(FOLLOW_12);
            rule__StateMachine__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4__2"


    // $ANTLR start "rule__StateMachine__Group_4__2__Impl"
    // InternalStateMachineNetwork.g:709:1: rule__StateMachine__Group_4__2__Impl : ( ( rule__StateMachine__Group_4_2__0 )? ) ;
    public final void rule__StateMachine__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:713:1: ( ( ( rule__StateMachine__Group_4_2__0 )? ) )
            // InternalStateMachineNetwork.g:714:1: ( ( rule__StateMachine__Group_4_2__0 )? )
            {
            // InternalStateMachineNetwork.g:714:1: ( ( rule__StateMachine__Group_4_2__0 )? )
            // InternalStateMachineNetwork.g:715:2: ( rule__StateMachine__Group_4_2__0 )?
            {
             before(grammarAccess.getStateMachineAccess().getGroup_4_2()); 
            // InternalStateMachineNetwork.g:716:2: ( rule__StateMachine__Group_4_2__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==19) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalStateMachineNetwork.g:716:3: rule__StateMachine__Group_4_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__StateMachine__Group_4_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStateMachineAccess().getGroup_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4__2__Impl"


    // $ANTLR start "rule__StateMachine__Group_4__3"
    // InternalStateMachineNetwork.g:724:1: rule__StateMachine__Group_4__3 : rule__StateMachine__Group_4__3__Impl ;
    public final void rule__StateMachine__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:728:1: ( rule__StateMachine__Group_4__3__Impl )
            // InternalStateMachineNetwork.g:729:2: rule__StateMachine__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4__3"


    // $ANTLR start "rule__StateMachine__Group_4__3__Impl"
    // InternalStateMachineNetwork.g:735:1: rule__StateMachine__Group_4__3__Impl : ( ( rule__StateMachine__Group_4_3__0 )? ) ;
    public final void rule__StateMachine__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:739:1: ( ( ( rule__StateMachine__Group_4_3__0 )? ) )
            // InternalStateMachineNetwork.g:740:1: ( ( rule__StateMachine__Group_4_3__0 )? )
            {
            // InternalStateMachineNetwork.g:740:1: ( ( rule__StateMachine__Group_4_3__0 )? )
            // InternalStateMachineNetwork.g:741:2: ( rule__StateMachine__Group_4_3__0 )?
            {
             before(grammarAccess.getStateMachineAccess().getGroup_4_3()); 
            // InternalStateMachineNetwork.g:742:2: ( rule__StateMachine__Group_4_3__0 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==20) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalStateMachineNetwork.g:742:3: rule__StateMachine__Group_4_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__StateMachine__Group_4_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStateMachineAccess().getGroup_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4__3__Impl"


    // $ANTLR start "rule__StateMachine__Group_4_2__0"
    // InternalStateMachineNetwork.g:751:1: rule__StateMachine__Group_4_2__0 : rule__StateMachine__Group_4_2__0__Impl rule__StateMachine__Group_4_2__1 ;
    public final void rule__StateMachine__Group_4_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:755:1: ( rule__StateMachine__Group_4_2__0__Impl rule__StateMachine__Group_4_2__1 )
            // InternalStateMachineNetwork.g:756:2: rule__StateMachine__Group_4_2__0__Impl rule__StateMachine__Group_4_2__1
            {
            pushFollow(FOLLOW_4);
            rule__StateMachine__Group_4_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_4_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4_2__0"


    // $ANTLR start "rule__StateMachine__Group_4_2__0__Impl"
    // InternalStateMachineNetwork.g:763:1: rule__StateMachine__Group_4_2__0__Impl : ( 'states:' ) ;
    public final void rule__StateMachine__Group_4_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:767:1: ( ( 'states:' ) )
            // InternalStateMachineNetwork.g:768:1: ( 'states:' )
            {
            // InternalStateMachineNetwork.g:768:1: ( 'states:' )
            // InternalStateMachineNetwork.g:769:2: 'states:'
            {
             before(grammarAccess.getStateMachineAccess().getStatesKeyword_4_2_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getStatesKeyword_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4_2__0__Impl"


    // $ANTLR start "rule__StateMachine__Group_4_2__1"
    // InternalStateMachineNetwork.g:778:1: rule__StateMachine__Group_4_2__1 : rule__StateMachine__Group_4_2__1__Impl ;
    public final void rule__StateMachine__Group_4_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:782:1: ( rule__StateMachine__Group_4_2__1__Impl )
            // InternalStateMachineNetwork.g:783:2: rule__StateMachine__Group_4_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_4_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4_2__1"


    // $ANTLR start "rule__StateMachine__Group_4_2__1__Impl"
    // InternalStateMachineNetwork.g:789:1: rule__StateMachine__Group_4_2__1__Impl : ( ( ( rule__StateMachine__StatesAssignment_4_2_1 ) ) ( ( rule__StateMachine__StatesAssignment_4_2_1 )* ) ) ;
    public final void rule__StateMachine__Group_4_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:793:1: ( ( ( ( rule__StateMachine__StatesAssignment_4_2_1 ) ) ( ( rule__StateMachine__StatesAssignment_4_2_1 )* ) ) )
            // InternalStateMachineNetwork.g:794:1: ( ( ( rule__StateMachine__StatesAssignment_4_2_1 ) ) ( ( rule__StateMachine__StatesAssignment_4_2_1 )* ) )
            {
            // InternalStateMachineNetwork.g:794:1: ( ( ( rule__StateMachine__StatesAssignment_4_2_1 ) ) ( ( rule__StateMachine__StatesAssignment_4_2_1 )* ) )
            // InternalStateMachineNetwork.g:795:2: ( ( rule__StateMachine__StatesAssignment_4_2_1 ) ) ( ( rule__StateMachine__StatesAssignment_4_2_1 )* )
            {
            // InternalStateMachineNetwork.g:795:2: ( ( rule__StateMachine__StatesAssignment_4_2_1 ) )
            // InternalStateMachineNetwork.g:796:3: ( rule__StateMachine__StatesAssignment_4_2_1 )
            {
             before(grammarAccess.getStateMachineAccess().getStatesAssignment_4_2_1()); 
            // InternalStateMachineNetwork.g:797:3: ( rule__StateMachine__StatesAssignment_4_2_1 )
            // InternalStateMachineNetwork.g:797:4: rule__StateMachine__StatesAssignment_4_2_1
            {
            pushFollow(FOLLOW_13);
            rule__StateMachine__StatesAssignment_4_2_1();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getStatesAssignment_4_2_1()); 

            }

            // InternalStateMachineNetwork.g:800:2: ( ( rule__StateMachine__StatesAssignment_4_2_1 )* )
            // InternalStateMachineNetwork.g:801:3: ( rule__StateMachine__StatesAssignment_4_2_1 )*
            {
             before(grammarAccess.getStateMachineAccess().getStatesAssignment_4_2_1()); 
            // InternalStateMachineNetwork.g:802:3: ( rule__StateMachine__StatesAssignment_4_2_1 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>=RULE_STRING && LA9_0<=RULE_ID)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalStateMachineNetwork.g:802:4: rule__StateMachine__StatesAssignment_4_2_1
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__StateMachine__StatesAssignment_4_2_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getStateMachineAccess().getStatesAssignment_4_2_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4_2__1__Impl"


    // $ANTLR start "rule__StateMachine__Group_4_3__0"
    // InternalStateMachineNetwork.g:812:1: rule__StateMachine__Group_4_3__0 : rule__StateMachine__Group_4_3__0__Impl rule__StateMachine__Group_4_3__1 ;
    public final void rule__StateMachine__Group_4_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:816:1: ( rule__StateMachine__Group_4_3__0__Impl rule__StateMachine__Group_4_3__1 )
            // InternalStateMachineNetwork.g:817:2: rule__StateMachine__Group_4_3__0__Impl rule__StateMachine__Group_4_3__1
            {
            pushFollow(FOLLOW_4);
            rule__StateMachine__Group_4_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_4_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4_3__0"


    // $ANTLR start "rule__StateMachine__Group_4_3__0__Impl"
    // InternalStateMachineNetwork.g:824:1: rule__StateMachine__Group_4_3__0__Impl : ( 'transitions:' ) ;
    public final void rule__StateMachine__Group_4_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:828:1: ( ( 'transitions:' ) )
            // InternalStateMachineNetwork.g:829:1: ( 'transitions:' )
            {
            // InternalStateMachineNetwork.g:829:1: ( 'transitions:' )
            // InternalStateMachineNetwork.g:830:2: 'transitions:'
            {
             before(grammarAccess.getStateMachineAccess().getTransitionsKeyword_4_3_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getTransitionsKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4_3__0__Impl"


    // $ANTLR start "rule__StateMachine__Group_4_3__1"
    // InternalStateMachineNetwork.g:839:1: rule__StateMachine__Group_4_3__1 : rule__StateMachine__Group_4_3__1__Impl ;
    public final void rule__StateMachine__Group_4_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:843:1: ( rule__StateMachine__Group_4_3__1__Impl )
            // InternalStateMachineNetwork.g:844:2: rule__StateMachine__Group_4_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_4_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4_3__1"


    // $ANTLR start "rule__StateMachine__Group_4_3__1__Impl"
    // InternalStateMachineNetwork.g:850:1: rule__StateMachine__Group_4_3__1__Impl : ( ( ( rule__StateMachine__TransitionsAssignment_4_3_1 ) ) ( ( rule__StateMachine__TransitionsAssignment_4_3_1 )* ) ) ;
    public final void rule__StateMachine__Group_4_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:854:1: ( ( ( ( rule__StateMachine__TransitionsAssignment_4_3_1 ) ) ( ( rule__StateMachine__TransitionsAssignment_4_3_1 )* ) ) )
            // InternalStateMachineNetwork.g:855:1: ( ( ( rule__StateMachine__TransitionsAssignment_4_3_1 ) ) ( ( rule__StateMachine__TransitionsAssignment_4_3_1 )* ) )
            {
            // InternalStateMachineNetwork.g:855:1: ( ( ( rule__StateMachine__TransitionsAssignment_4_3_1 ) ) ( ( rule__StateMachine__TransitionsAssignment_4_3_1 )* ) )
            // InternalStateMachineNetwork.g:856:2: ( ( rule__StateMachine__TransitionsAssignment_4_3_1 ) ) ( ( rule__StateMachine__TransitionsAssignment_4_3_1 )* )
            {
            // InternalStateMachineNetwork.g:856:2: ( ( rule__StateMachine__TransitionsAssignment_4_3_1 ) )
            // InternalStateMachineNetwork.g:857:3: ( rule__StateMachine__TransitionsAssignment_4_3_1 )
            {
             before(grammarAccess.getStateMachineAccess().getTransitionsAssignment_4_3_1()); 
            // InternalStateMachineNetwork.g:858:3: ( rule__StateMachine__TransitionsAssignment_4_3_1 )
            // InternalStateMachineNetwork.g:858:4: rule__StateMachine__TransitionsAssignment_4_3_1
            {
            pushFollow(FOLLOW_13);
            rule__StateMachine__TransitionsAssignment_4_3_1();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getTransitionsAssignment_4_3_1()); 

            }

            // InternalStateMachineNetwork.g:861:2: ( ( rule__StateMachine__TransitionsAssignment_4_3_1 )* )
            // InternalStateMachineNetwork.g:862:3: ( rule__StateMachine__TransitionsAssignment_4_3_1 )*
            {
             before(grammarAccess.getStateMachineAccess().getTransitionsAssignment_4_3_1()); 
            // InternalStateMachineNetwork.g:863:3: ( rule__StateMachine__TransitionsAssignment_4_3_1 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>=RULE_STRING && LA10_0<=RULE_ID)) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalStateMachineNetwork.g:863:4: rule__StateMachine__TransitionsAssignment_4_3_1
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__StateMachine__TransitionsAssignment_4_3_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getStateMachineAccess().getTransitionsAssignment_4_3_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_4_3__1__Impl"


    // $ANTLR start "rule__Channel__Group__0"
    // InternalStateMachineNetwork.g:873:1: rule__Channel__Group__0 : rule__Channel__Group__0__Impl rule__Channel__Group__1 ;
    public final void rule__Channel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:877:1: ( rule__Channel__Group__0__Impl rule__Channel__Group__1 )
            // InternalStateMachineNetwork.g:878:2: rule__Channel__Group__0__Impl rule__Channel__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Channel__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Channel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Channel__Group__0"


    // $ANTLR start "rule__Channel__Group__0__Impl"
    // InternalStateMachineNetwork.g:885:1: rule__Channel__Group__0__Impl : ( () ) ;
    public final void rule__Channel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:889:1: ( ( () ) )
            // InternalStateMachineNetwork.g:890:1: ( () )
            {
            // InternalStateMachineNetwork.g:890:1: ( () )
            // InternalStateMachineNetwork.g:891:2: ()
            {
             before(grammarAccess.getChannelAccess().getChannelAction_0()); 
            // InternalStateMachineNetwork.g:892:2: ()
            // InternalStateMachineNetwork.g:892:3: 
            {
            }

             after(grammarAccess.getChannelAccess().getChannelAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Channel__Group__0__Impl"


    // $ANTLR start "rule__Channel__Group__1"
    // InternalStateMachineNetwork.g:900:1: rule__Channel__Group__1 : rule__Channel__Group__1__Impl rule__Channel__Group__2 ;
    public final void rule__Channel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:904:1: ( rule__Channel__Group__1__Impl rule__Channel__Group__2 )
            // InternalStateMachineNetwork.g:905:2: rule__Channel__Group__1__Impl rule__Channel__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Channel__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Channel__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Channel__Group__1"


    // $ANTLR start "rule__Channel__Group__1__Impl"
    // InternalStateMachineNetwork.g:912:1: rule__Channel__Group__1__Impl : ( ( rule__Channel__Alternatives_1 ) ) ;
    public final void rule__Channel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:916:1: ( ( ( rule__Channel__Alternatives_1 ) ) )
            // InternalStateMachineNetwork.g:917:1: ( ( rule__Channel__Alternatives_1 ) )
            {
            // InternalStateMachineNetwork.g:917:1: ( ( rule__Channel__Alternatives_1 ) )
            // InternalStateMachineNetwork.g:918:2: ( rule__Channel__Alternatives_1 )
            {
             before(grammarAccess.getChannelAccess().getAlternatives_1()); 
            // InternalStateMachineNetwork.g:919:2: ( rule__Channel__Alternatives_1 )
            // InternalStateMachineNetwork.g:919:3: rule__Channel__Alternatives_1
            {
            pushFollow(FOLLOW_2);
            rule__Channel__Alternatives_1();

            state._fsp--;


            }

             after(grammarAccess.getChannelAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Channel__Group__1__Impl"


    // $ANTLR start "rule__Channel__Group__2"
    // InternalStateMachineNetwork.g:927:1: rule__Channel__Group__2 : rule__Channel__Group__2__Impl ;
    public final void rule__Channel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:931:1: ( rule__Channel__Group__2__Impl )
            // InternalStateMachineNetwork.g:932:2: rule__Channel__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Channel__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Channel__Group__2"


    // $ANTLR start "rule__Channel__Group__2__Impl"
    // InternalStateMachineNetwork.g:938:1: rule__Channel__Group__2__Impl : ( ( rule__Channel__NameAssignment_2 ) ) ;
    public final void rule__Channel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:942:1: ( ( ( rule__Channel__NameAssignment_2 ) ) )
            // InternalStateMachineNetwork.g:943:1: ( ( rule__Channel__NameAssignment_2 ) )
            {
            // InternalStateMachineNetwork.g:943:1: ( ( rule__Channel__NameAssignment_2 ) )
            // InternalStateMachineNetwork.g:944:2: ( rule__Channel__NameAssignment_2 )
            {
             before(grammarAccess.getChannelAccess().getNameAssignment_2()); 
            // InternalStateMachineNetwork.g:945:2: ( rule__Channel__NameAssignment_2 )
            // InternalStateMachineNetwork.g:945:3: rule__Channel__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Channel__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getChannelAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Channel__Group__2__Impl"


    // $ANTLR start "rule__State__Group__0"
    // InternalStateMachineNetwork.g:954:1: rule__State__Group__0 : rule__State__Group__0__Impl rule__State__Group__1 ;
    public final void rule__State__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:958:1: ( rule__State__Group__0__Impl rule__State__Group__1 )
            // InternalStateMachineNetwork.g:959:2: rule__State__Group__0__Impl rule__State__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__State__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__State__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__0"


    // $ANTLR start "rule__State__Group__0__Impl"
    // InternalStateMachineNetwork.g:966:1: rule__State__Group__0__Impl : ( () ) ;
    public final void rule__State__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:970:1: ( ( () ) )
            // InternalStateMachineNetwork.g:971:1: ( () )
            {
            // InternalStateMachineNetwork.g:971:1: ( () )
            // InternalStateMachineNetwork.g:972:2: ()
            {
             before(grammarAccess.getStateAccess().getStateAction_0()); 
            // InternalStateMachineNetwork.g:973:2: ()
            // InternalStateMachineNetwork.g:973:3: 
            {
            }

             after(grammarAccess.getStateAccess().getStateAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__0__Impl"


    // $ANTLR start "rule__State__Group__1"
    // InternalStateMachineNetwork.g:981:1: rule__State__Group__1 : rule__State__Group__1__Impl ;
    public final void rule__State__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:985:1: ( rule__State__Group__1__Impl )
            // InternalStateMachineNetwork.g:986:2: rule__State__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__State__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__1"


    // $ANTLR start "rule__State__Group__1__Impl"
    // InternalStateMachineNetwork.g:992:1: rule__State__Group__1__Impl : ( ( rule__State__NameAssignment_1 ) ) ;
    public final void rule__State__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:996:1: ( ( ( rule__State__NameAssignment_1 ) ) )
            // InternalStateMachineNetwork.g:997:1: ( ( rule__State__NameAssignment_1 ) )
            {
            // InternalStateMachineNetwork.g:997:1: ( ( rule__State__NameAssignment_1 ) )
            // InternalStateMachineNetwork.g:998:2: ( rule__State__NameAssignment_1 )
            {
             before(grammarAccess.getStateAccess().getNameAssignment_1()); 
            // InternalStateMachineNetwork.g:999:2: ( rule__State__NameAssignment_1 )
            // InternalStateMachineNetwork.g:999:3: rule__State__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__State__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStateAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__1__Impl"


    // $ANTLR start "rule__Transition__Group__0"
    // InternalStateMachineNetwork.g:1008:1: rule__Transition__Group__0 : rule__Transition__Group__0__Impl rule__Transition__Group__1 ;
    public final void rule__Transition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1012:1: ( rule__Transition__Group__0__Impl rule__Transition__Group__1 )
            // InternalStateMachineNetwork.g:1013:2: rule__Transition__Group__0__Impl rule__Transition__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Transition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0"


    // $ANTLR start "rule__Transition__Group__0__Impl"
    // InternalStateMachineNetwork.g:1020:1: rule__Transition__Group__0__Impl : ( () ) ;
    public final void rule__Transition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1024:1: ( ( () ) )
            // InternalStateMachineNetwork.g:1025:1: ( () )
            {
            // InternalStateMachineNetwork.g:1025:1: ( () )
            // InternalStateMachineNetwork.g:1026:2: ()
            {
             before(grammarAccess.getTransitionAccess().getTransitionAction_0()); 
            // InternalStateMachineNetwork.g:1027:2: ()
            // InternalStateMachineNetwork.g:1027:3: 
            {
            }

             after(grammarAccess.getTransitionAccess().getTransitionAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0__Impl"


    // $ANTLR start "rule__Transition__Group__1"
    // InternalStateMachineNetwork.g:1035:1: rule__Transition__Group__1 : rule__Transition__Group__1__Impl rule__Transition__Group__2 ;
    public final void rule__Transition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1039:1: ( rule__Transition__Group__1__Impl rule__Transition__Group__2 )
            // InternalStateMachineNetwork.g:1040:2: rule__Transition__Group__1__Impl rule__Transition__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__Transition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1"


    // $ANTLR start "rule__Transition__Group__1__Impl"
    // InternalStateMachineNetwork.g:1047:1: rule__Transition__Group__1__Impl : ( ( rule__Transition__SourceAssignment_1 ) ) ;
    public final void rule__Transition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1051:1: ( ( ( rule__Transition__SourceAssignment_1 ) ) )
            // InternalStateMachineNetwork.g:1052:1: ( ( rule__Transition__SourceAssignment_1 ) )
            {
            // InternalStateMachineNetwork.g:1052:1: ( ( rule__Transition__SourceAssignment_1 ) )
            // InternalStateMachineNetwork.g:1053:2: ( rule__Transition__SourceAssignment_1 )
            {
             before(grammarAccess.getTransitionAccess().getSourceAssignment_1()); 
            // InternalStateMachineNetwork.g:1054:2: ( rule__Transition__SourceAssignment_1 )
            // InternalStateMachineNetwork.g:1054:3: rule__Transition__SourceAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Transition__SourceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getSourceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1__Impl"


    // $ANTLR start "rule__Transition__Group__2"
    // InternalStateMachineNetwork.g:1062:1: rule__Transition__Group__2 : rule__Transition__Group__2__Impl rule__Transition__Group__3 ;
    public final void rule__Transition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1066:1: ( rule__Transition__Group__2__Impl rule__Transition__Group__3 )
            // InternalStateMachineNetwork.g:1067:2: rule__Transition__Group__2__Impl rule__Transition__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Transition__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2"


    // $ANTLR start "rule__Transition__Group__2__Impl"
    // InternalStateMachineNetwork.g:1074:1: rule__Transition__Group__2__Impl : ( '->' ) ;
    public final void rule__Transition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1078:1: ( ( '->' ) )
            // InternalStateMachineNetwork.g:1079:1: ( '->' )
            {
            // InternalStateMachineNetwork.g:1079:1: ( '->' )
            // InternalStateMachineNetwork.g:1080:2: '->'
            {
             before(grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_2()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__3"
    // InternalStateMachineNetwork.g:1089:1: rule__Transition__Group__3 : rule__Transition__Group__3__Impl rule__Transition__Group__4 ;
    public final void rule__Transition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1093:1: ( rule__Transition__Group__3__Impl rule__Transition__Group__4 )
            // InternalStateMachineNetwork.g:1094:2: rule__Transition__Group__3__Impl rule__Transition__Group__4
            {
            pushFollow(FOLLOW_16);
            rule__Transition__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3"


    // $ANTLR start "rule__Transition__Group__3__Impl"
    // InternalStateMachineNetwork.g:1101:1: rule__Transition__Group__3__Impl : ( ( rule__Transition__TargetAssignment_3 ) ) ;
    public final void rule__Transition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1105:1: ( ( ( rule__Transition__TargetAssignment_3 ) ) )
            // InternalStateMachineNetwork.g:1106:1: ( ( rule__Transition__TargetAssignment_3 ) )
            {
            // InternalStateMachineNetwork.g:1106:1: ( ( rule__Transition__TargetAssignment_3 ) )
            // InternalStateMachineNetwork.g:1107:2: ( rule__Transition__TargetAssignment_3 )
            {
             before(grammarAccess.getTransitionAccess().getTargetAssignment_3()); 
            // InternalStateMachineNetwork.g:1108:2: ( rule__Transition__TargetAssignment_3 )
            // InternalStateMachineNetwork.g:1108:3: rule__Transition__TargetAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Transition__TargetAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getTargetAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3__Impl"


    // $ANTLR start "rule__Transition__Group__4"
    // InternalStateMachineNetwork.g:1116:1: rule__Transition__Group__4 : rule__Transition__Group__4__Impl rule__Transition__Group__5 ;
    public final void rule__Transition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1120:1: ( rule__Transition__Group__4__Impl rule__Transition__Group__5 )
            // InternalStateMachineNetwork.g:1121:2: rule__Transition__Group__4__Impl rule__Transition__Group__5
            {
            pushFollow(FOLLOW_4);
            rule__Transition__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4"


    // $ANTLR start "rule__Transition__Group__4__Impl"
    // InternalStateMachineNetwork.g:1128:1: rule__Transition__Group__4__Impl : ( ( rule__Transition__Alternatives_4 ) ) ;
    public final void rule__Transition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1132:1: ( ( ( rule__Transition__Alternatives_4 ) ) )
            // InternalStateMachineNetwork.g:1133:1: ( ( rule__Transition__Alternatives_4 ) )
            {
            // InternalStateMachineNetwork.g:1133:1: ( ( rule__Transition__Alternatives_4 ) )
            // InternalStateMachineNetwork.g:1134:2: ( rule__Transition__Alternatives_4 )
            {
             before(grammarAccess.getTransitionAccess().getAlternatives_4()); 
            // InternalStateMachineNetwork.g:1135:2: ( rule__Transition__Alternatives_4 )
            // InternalStateMachineNetwork.g:1135:3: rule__Transition__Alternatives_4
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Alternatives_4();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getAlternatives_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4__Impl"


    // $ANTLR start "rule__Transition__Group__5"
    // InternalStateMachineNetwork.g:1143:1: rule__Transition__Group__5 : rule__Transition__Group__5__Impl ;
    public final void rule__Transition__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1147:1: ( rule__Transition__Group__5__Impl )
            // InternalStateMachineNetwork.g:1148:2: rule__Transition__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__5"


    // $ANTLR start "rule__Transition__Group__5__Impl"
    // InternalStateMachineNetwork.g:1154:1: rule__Transition__Group__5__Impl : ( ( rule__Transition__ChannelAssignment_5 ) ) ;
    public final void rule__Transition__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1158:1: ( ( ( rule__Transition__ChannelAssignment_5 ) ) )
            // InternalStateMachineNetwork.g:1159:1: ( ( rule__Transition__ChannelAssignment_5 ) )
            {
            // InternalStateMachineNetwork.g:1159:1: ( ( rule__Transition__ChannelAssignment_5 ) )
            // InternalStateMachineNetwork.g:1160:2: ( rule__Transition__ChannelAssignment_5 )
            {
             before(grammarAccess.getTransitionAccess().getChannelAssignment_5()); 
            // InternalStateMachineNetwork.g:1161:2: ( rule__Transition__ChannelAssignment_5 )
            // InternalStateMachineNetwork.g:1161:3: rule__Transition__ChannelAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Transition__ChannelAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getChannelAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__5__Impl"


    // $ANTLR start "rule__Network__NameAssignment_2"
    // InternalStateMachineNetwork.g:1170:1: rule__Network__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Network__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1174:1: ( ( ruleEString ) )
            // InternalStateMachineNetwork.g:1175:2: ( ruleEString )
            {
            // InternalStateMachineNetwork.g:1175:2: ( ruleEString )
            // InternalStateMachineNetwork.g:1176:3: ruleEString
            {
             before(grammarAccess.getNetworkAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getNetworkAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__NameAssignment_2"


    // $ANTLR start "rule__Network__ChannelsAssignment_5"
    // InternalStateMachineNetwork.g:1185:1: rule__Network__ChannelsAssignment_5 : ( ruleChannel ) ;
    public final void rule__Network__ChannelsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1189:1: ( ( ruleChannel ) )
            // InternalStateMachineNetwork.g:1190:2: ( ruleChannel )
            {
            // InternalStateMachineNetwork.g:1190:2: ( ruleChannel )
            // InternalStateMachineNetwork.g:1191:3: ruleChannel
            {
             before(grammarAccess.getNetworkAccess().getChannelsChannelParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleChannel();

            state._fsp--;

             after(grammarAccess.getNetworkAccess().getChannelsChannelParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__ChannelsAssignment_5"


    // $ANTLR start "rule__Network__StateMachinesAssignment_6"
    // InternalStateMachineNetwork.g:1200:1: rule__Network__StateMachinesAssignment_6 : ( ruleStateMachine ) ;
    public final void rule__Network__StateMachinesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1204:1: ( ( ruleStateMachine ) )
            // InternalStateMachineNetwork.g:1205:2: ( ruleStateMachine )
            {
            // InternalStateMachineNetwork.g:1205:2: ( ruleStateMachine )
            // InternalStateMachineNetwork.g:1206:3: ruleStateMachine
            {
             before(grammarAccess.getNetworkAccess().getStateMachinesStateMachineParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleStateMachine();

            state._fsp--;

             after(grammarAccess.getNetworkAccess().getStateMachinesStateMachineParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Network__StateMachinesAssignment_6"


    // $ANTLR start "rule__StateMachine__NameAssignment_2"
    // InternalStateMachineNetwork.g:1215:1: rule__StateMachine__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__StateMachine__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1219:1: ( ( ruleEString ) )
            // InternalStateMachineNetwork.g:1220:2: ( ruleEString )
            {
            // InternalStateMachineNetwork.g:1220:2: ( ruleEString )
            // InternalStateMachineNetwork.g:1221:3: ruleEString
            {
             before(grammarAccess.getStateMachineAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__NameAssignment_2"


    // $ANTLR start "rule__StateMachine__StatesAssignment_4_1"
    // InternalStateMachineNetwork.g:1230:1: rule__StateMachine__StatesAssignment_4_1 : ( ruleState ) ;
    public final void rule__StateMachine__StatesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1234:1: ( ( ruleState ) )
            // InternalStateMachineNetwork.g:1235:2: ( ruleState )
            {
            // InternalStateMachineNetwork.g:1235:2: ( ruleState )
            // InternalStateMachineNetwork.g:1236:3: ruleState
            {
             before(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleState();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__StatesAssignment_4_1"


    // $ANTLR start "rule__StateMachine__StatesAssignment_4_2_1"
    // InternalStateMachineNetwork.g:1245:1: rule__StateMachine__StatesAssignment_4_2_1 : ( ruleState ) ;
    public final void rule__StateMachine__StatesAssignment_4_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1249:1: ( ( ruleState ) )
            // InternalStateMachineNetwork.g:1250:2: ( ruleState )
            {
            // InternalStateMachineNetwork.g:1250:2: ( ruleState )
            // InternalStateMachineNetwork.g:1251:3: ruleState
            {
             before(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_4_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleState();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_4_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__StatesAssignment_4_2_1"


    // $ANTLR start "rule__StateMachine__TransitionsAssignment_4_3_1"
    // InternalStateMachineNetwork.g:1260:1: rule__StateMachine__TransitionsAssignment_4_3_1 : ( ruleTransition ) ;
    public final void rule__StateMachine__TransitionsAssignment_4_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1264:1: ( ( ruleTransition ) )
            // InternalStateMachineNetwork.g:1265:2: ( ruleTransition )
            {
            // InternalStateMachineNetwork.g:1265:2: ( ruleTransition )
            // InternalStateMachineNetwork.g:1266:3: ruleTransition
            {
             before(grammarAccess.getStateMachineAccess().getTransitionsTransitionParserRuleCall_4_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getTransitionsTransitionParserRuleCall_4_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__TransitionsAssignment_4_3_1"


    // $ANTLR start "rule__Channel__SynchronousAssignment_1_0"
    // InternalStateMachineNetwork.g:1275:1: rule__Channel__SynchronousAssignment_1_0 : ( ( 'synch' ) ) ;
    public final void rule__Channel__SynchronousAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1279:1: ( ( ( 'synch' ) ) )
            // InternalStateMachineNetwork.g:1280:2: ( ( 'synch' ) )
            {
            // InternalStateMachineNetwork.g:1280:2: ( ( 'synch' ) )
            // InternalStateMachineNetwork.g:1281:3: ( 'synch' )
            {
             before(grammarAccess.getChannelAccess().getSynchronousSynchKeyword_1_0_0()); 
            // InternalStateMachineNetwork.g:1282:3: ( 'synch' )
            // InternalStateMachineNetwork.g:1283:4: 'synch'
            {
             before(grammarAccess.getChannelAccess().getSynchronousSynchKeyword_1_0_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getChannelAccess().getSynchronousSynchKeyword_1_0_0()); 

            }

             after(grammarAccess.getChannelAccess().getSynchronousSynchKeyword_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Channel__SynchronousAssignment_1_0"


    // $ANTLR start "rule__Channel__NameAssignment_2"
    // InternalStateMachineNetwork.g:1294:1: rule__Channel__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Channel__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1298:1: ( ( ruleEString ) )
            // InternalStateMachineNetwork.g:1299:2: ( ruleEString )
            {
            // InternalStateMachineNetwork.g:1299:2: ( ruleEString )
            // InternalStateMachineNetwork.g:1300:3: ruleEString
            {
             before(grammarAccess.getChannelAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getChannelAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Channel__NameAssignment_2"


    // $ANTLR start "rule__State__NameAssignment_1"
    // InternalStateMachineNetwork.g:1309:1: rule__State__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__State__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1313:1: ( ( ruleEString ) )
            // InternalStateMachineNetwork.g:1314:2: ( ruleEString )
            {
            // InternalStateMachineNetwork.g:1314:2: ( ruleEString )
            // InternalStateMachineNetwork.g:1315:3: ruleEString
            {
             before(grammarAccess.getStateAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getStateAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__NameAssignment_1"


    // $ANTLR start "rule__Transition__SourceAssignment_1"
    // InternalStateMachineNetwork.g:1324:1: rule__Transition__SourceAssignment_1 : ( ( ruleEString ) ) ;
    public final void rule__Transition__SourceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1328:1: ( ( ( ruleEString ) ) )
            // InternalStateMachineNetwork.g:1329:2: ( ( ruleEString ) )
            {
            // InternalStateMachineNetwork.g:1329:2: ( ( ruleEString ) )
            // InternalStateMachineNetwork.g:1330:3: ( ruleEString )
            {
             before(grammarAccess.getTransitionAccess().getSourceStateCrossReference_1_0()); 
            // InternalStateMachineNetwork.g:1331:3: ( ruleEString )
            // InternalStateMachineNetwork.g:1332:4: ruleEString
            {
             before(grammarAccess.getTransitionAccess().getSourceStateEStringParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTransitionAccess().getSourceStateEStringParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getSourceStateCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__SourceAssignment_1"


    // $ANTLR start "rule__Transition__TargetAssignment_3"
    // InternalStateMachineNetwork.g:1343:1: rule__Transition__TargetAssignment_3 : ( ( ruleEString ) ) ;
    public final void rule__Transition__TargetAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1347:1: ( ( ( ruleEString ) ) )
            // InternalStateMachineNetwork.g:1348:2: ( ( ruleEString ) )
            {
            // InternalStateMachineNetwork.g:1348:2: ( ( ruleEString ) )
            // InternalStateMachineNetwork.g:1349:3: ( ruleEString )
            {
             before(grammarAccess.getTransitionAccess().getTargetStateCrossReference_3_0()); 
            // InternalStateMachineNetwork.g:1350:3: ( ruleEString )
            // InternalStateMachineNetwork.g:1351:4: ruleEString
            {
             before(grammarAccess.getTransitionAccess().getTargetStateEStringParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTransitionAccess().getTargetStateEStringParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getTargetStateCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__TargetAssignment_3"


    // $ANTLR start "rule__Transition__SendingAssignment_4_0"
    // InternalStateMachineNetwork.g:1362:1: rule__Transition__SendingAssignment_4_0 : ( ( 'send' ) ) ;
    public final void rule__Transition__SendingAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1366:1: ( ( ( 'send' ) ) )
            // InternalStateMachineNetwork.g:1367:2: ( ( 'send' ) )
            {
            // InternalStateMachineNetwork.g:1367:2: ( ( 'send' ) )
            // InternalStateMachineNetwork.g:1368:3: ( 'send' )
            {
             before(grammarAccess.getTransitionAccess().getSendingSendKeyword_4_0_0()); 
            // InternalStateMachineNetwork.g:1369:3: ( 'send' )
            // InternalStateMachineNetwork.g:1370:4: 'send'
            {
             before(grammarAccess.getTransitionAccess().getSendingSendKeyword_4_0_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getSendingSendKeyword_4_0_0()); 

            }

             after(grammarAccess.getTransitionAccess().getSendingSendKeyword_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__SendingAssignment_4_0"


    // $ANTLR start "rule__Transition__ChannelAssignment_5"
    // InternalStateMachineNetwork.g:1381:1: rule__Transition__ChannelAssignment_5 : ( ( ruleEString ) ) ;
    public final void rule__Transition__ChannelAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalStateMachineNetwork.g:1385:1: ( ( ( ruleEString ) ) )
            // InternalStateMachineNetwork.g:1386:2: ( ( ruleEString ) )
            {
            // InternalStateMachineNetwork.g:1386:2: ( ( ruleEString ) )
            // InternalStateMachineNetwork.g:1387:3: ( ruleEString )
            {
             before(grammarAccess.getTransitionAccess().getChannelChannelCrossReference_5_0()); 
            // InternalStateMachineNetwork.g:1388:3: ( ruleEString )
            // InternalStateMachineNetwork.g:1389:4: ruleEString
            {
             before(grammarAccess.getTransitionAccess().getChannelChannelEStringParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTransitionAccess().getChannelChannelEStringParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getChannelChannelCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__ChannelAssignment_5"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000430800L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000400802L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000050000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400800L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000801000L});

}