package de.luh.statemachine.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import de.luh.statemachine.services.StateMachineNetworkGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalStateMachineNetworkParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Network'", "'{'", "'channel declarations:'", "'}'", "'StateMachine'", "'initial state:'", "'states:'", "'transitions:'", "'synch'", "'asynch'", "'->'", "'send'", "'receive'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalStateMachineNetworkParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalStateMachineNetworkParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalStateMachineNetworkParser.tokenNames; }
    public String getGrammarFileName() { return "InternalStateMachineNetwork.g"; }



     	private StateMachineNetworkGrammarAccess grammarAccess;

        public InternalStateMachineNetworkParser(TokenStream input, StateMachineNetworkGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Network";
       	}

       	@Override
       	protected StateMachineNetworkGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleNetwork"
    // InternalStateMachineNetwork.g:64:1: entryRuleNetwork returns [EObject current=null] : iv_ruleNetwork= ruleNetwork EOF ;
    public final EObject entryRuleNetwork() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNetwork = null;


        try {
            // InternalStateMachineNetwork.g:64:48: (iv_ruleNetwork= ruleNetwork EOF )
            // InternalStateMachineNetwork.g:65:2: iv_ruleNetwork= ruleNetwork EOF
            {
             newCompositeNode(grammarAccess.getNetworkRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNetwork=ruleNetwork();

            state._fsp--;

             current =iv_ruleNetwork; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNetwork"


    // $ANTLR start "ruleNetwork"
    // InternalStateMachineNetwork.g:71:1: ruleNetwork returns [EObject current=null] : ( () otherlv_1= 'Network' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'channel declarations:' ( (lv_channels_5_0= ruleChannel ) )* ( (lv_stateMachines_6_0= ruleStateMachine ) )* otherlv_7= '}' ) ;
    public final EObject ruleNetwork() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_channels_5_0 = null;

        EObject lv_stateMachines_6_0 = null;



        	enterRule();

        try {
            // InternalStateMachineNetwork.g:77:2: ( ( () otherlv_1= 'Network' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'channel declarations:' ( (lv_channels_5_0= ruleChannel ) )* ( (lv_stateMachines_6_0= ruleStateMachine ) )* otherlv_7= '}' ) )
            // InternalStateMachineNetwork.g:78:2: ( () otherlv_1= 'Network' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'channel declarations:' ( (lv_channels_5_0= ruleChannel ) )* ( (lv_stateMachines_6_0= ruleStateMachine ) )* otherlv_7= '}' )
            {
            // InternalStateMachineNetwork.g:78:2: ( () otherlv_1= 'Network' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'channel declarations:' ( (lv_channels_5_0= ruleChannel ) )* ( (lv_stateMachines_6_0= ruleStateMachine ) )* otherlv_7= '}' )
            // InternalStateMachineNetwork.g:79:3: () otherlv_1= 'Network' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'channel declarations:' ( (lv_channels_5_0= ruleChannel ) )* ( (lv_stateMachines_6_0= ruleStateMachine ) )* otherlv_7= '}'
            {
            // InternalStateMachineNetwork.g:79:3: ()
            // InternalStateMachineNetwork.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getNetworkAccess().getNetworkAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getNetworkAccess().getNetworkKeyword_1());
            		
            // InternalStateMachineNetwork.g:90:3: ( (lv_name_2_0= ruleEString ) )
            // InternalStateMachineNetwork.g:91:4: (lv_name_2_0= ruleEString )
            {
            // InternalStateMachineNetwork.g:91:4: (lv_name_2_0= ruleEString )
            // InternalStateMachineNetwork.g:92:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getNetworkAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNetworkRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"de.luh.statemachine.StateMachineNetwork.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getNetworkAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,13,FOLLOW_6); 

            			newLeafNode(otherlv_4, grammarAccess.getNetworkAccess().getChannelDeclarationsKeyword_4());
            		
            // InternalStateMachineNetwork.g:117:3: ( (lv_channels_5_0= ruleChannel ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=19 && LA1_0<=20)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalStateMachineNetwork.g:118:4: (lv_channels_5_0= ruleChannel )
            	    {
            	    // InternalStateMachineNetwork.g:118:4: (lv_channels_5_0= ruleChannel )
            	    // InternalStateMachineNetwork.g:119:5: lv_channels_5_0= ruleChannel
            	    {

            	    					newCompositeNode(grammarAccess.getNetworkAccess().getChannelsChannelParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_channels_5_0=ruleChannel();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getNetworkRule());
            	    					}
            	    					add(
            	    						current,
            	    						"channels",
            	    						lv_channels_5_0,
            	    						"de.luh.statemachine.StateMachineNetwork.Channel");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalStateMachineNetwork.g:136:3: ( (lv_stateMachines_6_0= ruleStateMachine ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==15) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalStateMachineNetwork.g:137:4: (lv_stateMachines_6_0= ruleStateMachine )
            	    {
            	    // InternalStateMachineNetwork.g:137:4: (lv_stateMachines_6_0= ruleStateMachine )
            	    // InternalStateMachineNetwork.g:138:5: lv_stateMachines_6_0= ruleStateMachine
            	    {

            	    					newCompositeNode(grammarAccess.getNetworkAccess().getStateMachinesStateMachineParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_stateMachines_6_0=ruleStateMachine();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getNetworkRule());
            	    					}
            	    					add(
            	    						current,
            	    						"stateMachines",
            	    						lv_stateMachines_6_0,
            	    						"de.luh.statemachine.StateMachineNetwork.StateMachine");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_7=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getNetworkAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNetwork"


    // $ANTLR start "entryRuleEString"
    // InternalStateMachineNetwork.g:163:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalStateMachineNetwork.g:163:47: (iv_ruleEString= ruleEString EOF )
            // InternalStateMachineNetwork.g:164:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalStateMachineNetwork.g:170:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalStateMachineNetwork.g:176:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalStateMachineNetwork.g:177:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalStateMachineNetwork.g:177:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_STRING) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalStateMachineNetwork.g:178:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalStateMachineNetwork.g:186:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleStateMachine"
    // InternalStateMachineNetwork.g:197:1: entryRuleStateMachine returns [EObject current=null] : iv_ruleStateMachine= ruleStateMachine EOF ;
    public final EObject entryRuleStateMachine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStateMachine = null;


        try {
            // InternalStateMachineNetwork.g:197:53: (iv_ruleStateMachine= ruleStateMachine EOF )
            // InternalStateMachineNetwork.g:198:2: iv_ruleStateMachine= ruleStateMachine EOF
            {
             newCompositeNode(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStateMachine=ruleStateMachine();

            state._fsp--;

             current =iv_ruleStateMachine; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalStateMachineNetwork.g:204:1: ruleStateMachine returns [EObject current=null] : ( () otherlv_1= 'StateMachine' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'initial state:' ( (lv_states_5_0= ruleState ) ) (otherlv_6= 'states:' ( (lv_states_7_0= ruleState ) )+ )? (otherlv_8= 'transitions:' ( (lv_transitions_9_0= ruleTransition ) )+ )? )? otherlv_10= '}' ) ;
    public final EObject ruleStateMachine() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_states_5_0 = null;

        EObject lv_states_7_0 = null;

        EObject lv_transitions_9_0 = null;



        	enterRule();

        try {
            // InternalStateMachineNetwork.g:210:2: ( ( () otherlv_1= 'StateMachine' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'initial state:' ( (lv_states_5_0= ruleState ) ) (otherlv_6= 'states:' ( (lv_states_7_0= ruleState ) )+ )? (otherlv_8= 'transitions:' ( (lv_transitions_9_0= ruleTransition ) )+ )? )? otherlv_10= '}' ) )
            // InternalStateMachineNetwork.g:211:2: ( () otherlv_1= 'StateMachine' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'initial state:' ( (lv_states_5_0= ruleState ) ) (otherlv_6= 'states:' ( (lv_states_7_0= ruleState ) )+ )? (otherlv_8= 'transitions:' ( (lv_transitions_9_0= ruleTransition ) )+ )? )? otherlv_10= '}' )
            {
            // InternalStateMachineNetwork.g:211:2: ( () otherlv_1= 'StateMachine' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'initial state:' ( (lv_states_5_0= ruleState ) ) (otherlv_6= 'states:' ( (lv_states_7_0= ruleState ) )+ )? (otherlv_8= 'transitions:' ( (lv_transitions_9_0= ruleTransition ) )+ )? )? otherlv_10= '}' )
            // InternalStateMachineNetwork.g:212:3: () otherlv_1= 'StateMachine' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'initial state:' ( (lv_states_5_0= ruleState ) ) (otherlv_6= 'states:' ( (lv_states_7_0= ruleState ) )+ )? (otherlv_8= 'transitions:' ( (lv_transitions_9_0= ruleTransition ) )+ )? )? otherlv_10= '}'
            {
            // InternalStateMachineNetwork.g:212:3: ()
            // InternalStateMachineNetwork.g:213:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStateMachineAccess().getStateMachineAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,15,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getStateMachineAccess().getStateMachineKeyword_1());
            		
            // InternalStateMachineNetwork.g:223:3: ( (lv_name_2_0= ruleEString ) )
            // InternalStateMachineNetwork.g:224:4: (lv_name_2_0= ruleEString )
            {
            // InternalStateMachineNetwork.g:224:4: (lv_name_2_0= ruleEString )
            // InternalStateMachineNetwork.g:225:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStateMachineAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStateMachineRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"de.luh.statemachine.StateMachineNetwork.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_8); 

            			newLeafNode(otherlv_3, grammarAccess.getStateMachineAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalStateMachineNetwork.g:246:3: (otherlv_4= 'initial state:' ( (lv_states_5_0= ruleState ) ) (otherlv_6= 'states:' ( (lv_states_7_0= ruleState ) )+ )? (otherlv_8= 'transitions:' ( (lv_transitions_9_0= ruleTransition ) )+ )? )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==16) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalStateMachineNetwork.g:247:4: otherlv_4= 'initial state:' ( (lv_states_5_0= ruleState ) ) (otherlv_6= 'states:' ( (lv_states_7_0= ruleState ) )+ )? (otherlv_8= 'transitions:' ( (lv_transitions_9_0= ruleTransition ) )+ )?
                    {
                    otherlv_4=(Token)match(input,16,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getStateMachineAccess().getInitialStateKeyword_4_0());
                    			
                    // InternalStateMachineNetwork.g:251:4: ( (lv_states_5_0= ruleState ) )
                    // InternalStateMachineNetwork.g:252:5: (lv_states_5_0= ruleState )
                    {
                    // InternalStateMachineNetwork.g:252:5: (lv_states_5_0= ruleState )
                    // InternalStateMachineNetwork.g:253:6: lv_states_5_0= ruleState
                    {

                    						newCompositeNode(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_states_5_0=ruleState();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getStateMachineRule());
                    						}
                    						add(
                    							current,
                    							"states",
                    							lv_states_5_0,
                    							"de.luh.statemachine.StateMachineNetwork.State");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalStateMachineNetwork.g:270:4: (otherlv_6= 'states:' ( (lv_states_7_0= ruleState ) )+ )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0==17) ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // InternalStateMachineNetwork.g:271:5: otherlv_6= 'states:' ( (lv_states_7_0= ruleState ) )+
                            {
                            otherlv_6=(Token)match(input,17,FOLLOW_3); 

                            					newLeafNode(otherlv_6, grammarAccess.getStateMachineAccess().getStatesKeyword_4_2_0());
                            				
                            // InternalStateMachineNetwork.g:275:5: ( (lv_states_7_0= ruleState ) )+
                            int cnt4=0;
                            loop4:
                            do {
                                int alt4=2;
                                int LA4_0 = input.LA(1);

                                if ( ((LA4_0>=RULE_STRING && LA4_0<=RULE_ID)) ) {
                                    alt4=1;
                                }


                                switch (alt4) {
                            	case 1 :
                            	    // InternalStateMachineNetwork.g:276:6: (lv_states_7_0= ruleState )
                            	    {
                            	    // InternalStateMachineNetwork.g:276:6: (lv_states_7_0= ruleState )
                            	    // InternalStateMachineNetwork.g:277:7: lv_states_7_0= ruleState
                            	    {

                            	    							newCompositeNode(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_4_2_1_0());
                            	    						
                            	    pushFollow(FOLLOW_10);
                            	    lv_states_7_0=ruleState();

                            	    state._fsp--;


                            	    							if (current==null) {
                            	    								current = createModelElementForParent(grammarAccess.getStateMachineRule());
                            	    							}
                            	    							add(
                            	    								current,
                            	    								"states",
                            	    								lv_states_7_0,
                            	    								"de.luh.statemachine.StateMachineNetwork.State");
                            	    							afterParserOrEnumRuleCall();
                            	    						

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    if ( cnt4 >= 1 ) break loop4;
                                        EarlyExitException eee =
                                            new EarlyExitException(4, input);
                                        throw eee;
                                }
                                cnt4++;
                            } while (true);


                            }
                            break;

                    }

                    // InternalStateMachineNetwork.g:295:4: (otherlv_8= 'transitions:' ( (lv_transitions_9_0= ruleTransition ) )+ )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==18) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalStateMachineNetwork.g:296:5: otherlv_8= 'transitions:' ( (lv_transitions_9_0= ruleTransition ) )+
                            {
                            otherlv_8=(Token)match(input,18,FOLLOW_3); 

                            					newLeafNode(otherlv_8, grammarAccess.getStateMachineAccess().getTransitionsKeyword_4_3_0());
                            				
                            // InternalStateMachineNetwork.g:300:5: ( (lv_transitions_9_0= ruleTransition ) )+
                            int cnt6=0;
                            loop6:
                            do {
                                int alt6=2;
                                int LA6_0 = input.LA(1);

                                if ( ((LA6_0>=RULE_STRING && LA6_0<=RULE_ID)) ) {
                                    alt6=1;
                                }


                                switch (alt6) {
                            	case 1 :
                            	    // InternalStateMachineNetwork.g:301:6: (lv_transitions_9_0= ruleTransition )
                            	    {
                            	    // InternalStateMachineNetwork.g:301:6: (lv_transitions_9_0= ruleTransition )
                            	    // InternalStateMachineNetwork.g:302:7: lv_transitions_9_0= ruleTransition
                            	    {

                            	    							newCompositeNode(grammarAccess.getStateMachineAccess().getTransitionsTransitionParserRuleCall_4_3_1_0());
                            	    						
                            	    pushFollow(FOLLOW_11);
                            	    lv_transitions_9_0=ruleTransition();

                            	    state._fsp--;


                            	    							if (current==null) {
                            	    								current = createModelElementForParent(grammarAccess.getStateMachineRule());
                            	    							}
                            	    							add(
                            	    								current,
                            	    								"transitions",
                            	    								lv_transitions_9_0,
                            	    								"de.luh.statemachine.StateMachineNetwork.Transition");
                            	    							afterParserOrEnumRuleCall();
                            	    						

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    if ( cnt6 >= 1 ) break loop6;
                                        EarlyExitException eee =
                                            new EarlyExitException(6, input);
                                        throw eee;
                                }
                                cnt6++;
                            } while (true);


                            }
                            break;

                    }


                    }
                    break;

            }

            otherlv_10=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getStateMachineAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleChannel"
    // InternalStateMachineNetwork.g:329:1: entryRuleChannel returns [EObject current=null] : iv_ruleChannel= ruleChannel EOF ;
    public final EObject entryRuleChannel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChannel = null;


        try {
            // InternalStateMachineNetwork.g:329:48: (iv_ruleChannel= ruleChannel EOF )
            // InternalStateMachineNetwork.g:330:2: iv_ruleChannel= ruleChannel EOF
            {
             newCompositeNode(grammarAccess.getChannelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleChannel=ruleChannel();

            state._fsp--;

             current =iv_ruleChannel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChannel"


    // $ANTLR start "ruleChannel"
    // InternalStateMachineNetwork.g:336:1: ruleChannel returns [EObject current=null] : ( () ( ( (lv_synchronous_1_0= 'synch' ) ) | otherlv_2= 'asynch' ) ( (lv_name_3_0= ruleEString ) ) ) ;
    public final EObject ruleChannel() throws RecognitionException {
        EObject current = null;

        Token lv_synchronous_1_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;



        	enterRule();

        try {
            // InternalStateMachineNetwork.g:342:2: ( ( () ( ( (lv_synchronous_1_0= 'synch' ) ) | otherlv_2= 'asynch' ) ( (lv_name_3_0= ruleEString ) ) ) )
            // InternalStateMachineNetwork.g:343:2: ( () ( ( (lv_synchronous_1_0= 'synch' ) ) | otherlv_2= 'asynch' ) ( (lv_name_3_0= ruleEString ) ) )
            {
            // InternalStateMachineNetwork.g:343:2: ( () ( ( (lv_synchronous_1_0= 'synch' ) ) | otherlv_2= 'asynch' ) ( (lv_name_3_0= ruleEString ) ) )
            // InternalStateMachineNetwork.g:344:3: () ( ( (lv_synchronous_1_0= 'synch' ) ) | otherlv_2= 'asynch' ) ( (lv_name_3_0= ruleEString ) )
            {
            // InternalStateMachineNetwork.g:344:3: ()
            // InternalStateMachineNetwork.g:345:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getChannelAccess().getChannelAction_0(),
            					current);
            			

            }

            // InternalStateMachineNetwork.g:351:3: ( ( (lv_synchronous_1_0= 'synch' ) ) | otherlv_2= 'asynch' )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==19) ) {
                alt9=1;
            }
            else if ( (LA9_0==20) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalStateMachineNetwork.g:352:4: ( (lv_synchronous_1_0= 'synch' ) )
                    {
                    // InternalStateMachineNetwork.g:352:4: ( (lv_synchronous_1_0= 'synch' ) )
                    // InternalStateMachineNetwork.g:353:5: (lv_synchronous_1_0= 'synch' )
                    {
                    // InternalStateMachineNetwork.g:353:5: (lv_synchronous_1_0= 'synch' )
                    // InternalStateMachineNetwork.g:354:6: lv_synchronous_1_0= 'synch'
                    {
                    lv_synchronous_1_0=(Token)match(input,19,FOLLOW_3); 

                    						newLeafNode(lv_synchronous_1_0, grammarAccess.getChannelAccess().getSynchronousSynchKeyword_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getChannelRule());
                    						}
                    						setWithLastConsumed(current, "synchronous", true, "synch");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalStateMachineNetwork.g:367:4: otherlv_2= 'asynch'
                    {
                    otherlv_2=(Token)match(input,20,FOLLOW_3); 

                    				newLeafNode(otherlv_2, grammarAccess.getChannelAccess().getAsynchKeyword_1_1());
                    			

                    }
                    break;

            }

            // InternalStateMachineNetwork.g:372:3: ( (lv_name_3_0= ruleEString ) )
            // InternalStateMachineNetwork.g:373:4: (lv_name_3_0= ruleEString )
            {
            // InternalStateMachineNetwork.g:373:4: (lv_name_3_0= ruleEString )
            // InternalStateMachineNetwork.g:374:5: lv_name_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getChannelAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getChannelRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_3_0,
            						"de.luh.statemachine.StateMachineNetwork.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChannel"


    // $ANTLR start "entryRuleState"
    // InternalStateMachineNetwork.g:395:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalStateMachineNetwork.g:395:46: (iv_ruleState= ruleState EOF )
            // InternalStateMachineNetwork.g:396:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalStateMachineNetwork.g:402:1: ruleState returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalStateMachineNetwork.g:408:2: ( ( () ( (lv_name_1_0= ruleEString ) ) ) )
            // InternalStateMachineNetwork.g:409:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            {
            // InternalStateMachineNetwork.g:409:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            // InternalStateMachineNetwork.g:410:3: () ( (lv_name_1_0= ruleEString ) )
            {
            // InternalStateMachineNetwork.g:410:3: ()
            // InternalStateMachineNetwork.g:411:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStateAccess().getStateAction_0(),
            					current);
            			

            }

            // InternalStateMachineNetwork.g:417:3: ( (lv_name_1_0= ruleEString ) )
            // InternalStateMachineNetwork.g:418:4: (lv_name_1_0= ruleEString )
            {
            // InternalStateMachineNetwork.g:418:4: (lv_name_1_0= ruleEString )
            // InternalStateMachineNetwork.g:419:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStateAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStateRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"de.luh.statemachine.StateMachineNetwork.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalStateMachineNetwork.g:440:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalStateMachineNetwork.g:440:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalStateMachineNetwork.g:441:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalStateMachineNetwork.g:447:1: ruleTransition returns [EObject current=null] : ( () ( ( ruleEString ) ) otherlv_2= '->' ( ( ruleEString ) ) ( ( (lv_sending_4_0= 'send' ) ) | otherlv_5= 'receive' ) ( ( ruleEString ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_sending_4_0=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalStateMachineNetwork.g:453:2: ( ( () ( ( ruleEString ) ) otherlv_2= '->' ( ( ruleEString ) ) ( ( (lv_sending_4_0= 'send' ) ) | otherlv_5= 'receive' ) ( ( ruleEString ) ) ) )
            // InternalStateMachineNetwork.g:454:2: ( () ( ( ruleEString ) ) otherlv_2= '->' ( ( ruleEString ) ) ( ( (lv_sending_4_0= 'send' ) ) | otherlv_5= 'receive' ) ( ( ruleEString ) ) )
            {
            // InternalStateMachineNetwork.g:454:2: ( () ( ( ruleEString ) ) otherlv_2= '->' ( ( ruleEString ) ) ( ( (lv_sending_4_0= 'send' ) ) | otherlv_5= 'receive' ) ( ( ruleEString ) ) )
            // InternalStateMachineNetwork.g:455:3: () ( ( ruleEString ) ) otherlv_2= '->' ( ( ruleEString ) ) ( ( (lv_sending_4_0= 'send' ) ) | otherlv_5= 'receive' ) ( ( ruleEString ) )
            {
            // InternalStateMachineNetwork.g:455:3: ()
            // InternalStateMachineNetwork.g:456:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTransitionAccess().getTransitionAction_0(),
            					current);
            			

            }

            // InternalStateMachineNetwork.g:462:3: ( ( ruleEString ) )
            // InternalStateMachineNetwork.g:463:4: ( ruleEString )
            {
            // InternalStateMachineNetwork.g:463:4: ( ruleEString )
            // InternalStateMachineNetwork.g:464:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTransitionAccess().getSourceStateCrossReference_1_0());
            				
            pushFollow(FOLLOW_12);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_2());
            		
            // InternalStateMachineNetwork.g:482:3: ( ( ruleEString ) )
            // InternalStateMachineNetwork.g:483:4: ( ruleEString )
            {
            // InternalStateMachineNetwork.g:483:4: ( ruleEString )
            // InternalStateMachineNetwork.g:484:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTransitionAccess().getTargetStateCrossReference_3_0());
            				
            pushFollow(FOLLOW_13);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalStateMachineNetwork.g:498:3: ( ( (lv_sending_4_0= 'send' ) ) | otherlv_5= 'receive' )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==22) ) {
                alt10=1;
            }
            else if ( (LA10_0==23) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalStateMachineNetwork.g:499:4: ( (lv_sending_4_0= 'send' ) )
                    {
                    // InternalStateMachineNetwork.g:499:4: ( (lv_sending_4_0= 'send' ) )
                    // InternalStateMachineNetwork.g:500:5: (lv_sending_4_0= 'send' )
                    {
                    // InternalStateMachineNetwork.g:500:5: (lv_sending_4_0= 'send' )
                    // InternalStateMachineNetwork.g:501:6: lv_sending_4_0= 'send'
                    {
                    lv_sending_4_0=(Token)match(input,22,FOLLOW_3); 

                    						newLeafNode(lv_sending_4_0, grammarAccess.getTransitionAccess().getSendingSendKeyword_4_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTransitionRule());
                    						}
                    						setWithLastConsumed(current, "sending", true, "send");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalStateMachineNetwork.g:514:4: otherlv_5= 'receive'
                    {
                    otherlv_5=(Token)match(input,23,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getTransitionAccess().getReceiveKeyword_4_1());
                    			

                    }
                    break;

            }

            // InternalStateMachineNetwork.g:519:3: ( ( ruleEString ) )
            // InternalStateMachineNetwork.g:520:4: ( ruleEString )
            {
            // InternalStateMachineNetwork.g:520:4: ( ruleEString )
            // InternalStateMachineNetwork.g:521:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTransitionAccess().getChannelChannelCrossReference_5_0());
            				
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000018C000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000000C000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000014000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000064000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000044030L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000004030L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000C00000L});

}