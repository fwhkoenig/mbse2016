package de.luh.statemachine.design.xtext;


import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.XtextResource;

import statemachine.Channel;
import statemachine.Network;
import statemachine.StatemachineFactory;
import statemachine.Transition;

public class StatemachineServices {

	public EObject openTextEditor(EObject any) {
		if (any != null && any.eResource() instanceof XtextResource && any.eResource().getURI() != null) {

			String fileURI = any.eResource().getURI().toPlatformString(true);
			IFile workspaceFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(fileURI));
			if (workspaceFile != null) {
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					IEditorPart openEditor = IDE.openEditor(page, workspaceFile,
							"de.luh.statemachine.StateMachineNetwork", true);
					if (openEditor instanceof AbstractTextEditor) {
						ICompositeNode node = NodeModelUtils.findActualNodeFor(any);
						if (node != null) {
							int offset = node.getOffset();
							int length = node.getTotalEndOffset() - offset;
							((AbstractTextEditor) openEditor).selectAndReveal(offset, length);
							System.out.println("StatemachineServices.openTextEditor()");
						}
					}
					// editorInput.
				} catch (PartInitException e) {
					// Put your exception handler here if you wish to.
				}
			}
		}
		System.out.println(any);
		return any;
	}

	public EObject setChannelForTransition(EObject any) {
		if (any instanceof Transition) {
			Transition transition = (Transition) any;
			Network network = getContainingNetwork(transition);
			Channel channel = null;
			if (network.getChannels().isEmpty()) {
				channel = StatemachineFactory.eINSTANCE.createChannel();
				channel.setName("newChannel");
				channel.setSynchronous(false);
				network.getChannels().add(channel);
			} else {
				channel = network.getChannels().get(0);
			}
			transition.setChannel(channel);
		}

		return any;
	}

	private Network getContainingNetwork(Transition transition) {
		EObject container = transition.eContainer();
		while (container != null) {
			if (container instanceof Network) {
				return (Network) container;
			}
			container = container.eContainer();
		}
		return null;
	}

}
