/**
 */
package statemachine.interpreter;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see statemachine.interpreter.InterpreterFactory
 * @model kind="package"
 * @generated
 */
public interface InterpreterPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "interpreter";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/statemachine/interpreter";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "interpreter";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InterpreterPackage eINSTANCE = statemachine.interpreter.impl.InterpreterPackageImpl.init();

	/**
	 * The meta object id for the '{@link statemachine.interpreter.impl.NetworkStateImpl <em>Network State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.interpreter.impl.NetworkStateImpl
	 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getNetworkState()
	 * @generated
	 */
	int NETWORK_STATE = 0;

	/**
	 * The feature id for the '<em><b>Available Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_STATE__AVAILABLE_TRANSITIONS = 0;

	/**
	 * The feature id for the '<em><b>Channel States</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_STATE__CHANNEL_STATES = 1;

	/**
	 * The feature id for the '<em><b>Statemachine States</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_STATE__STATEMACHINE_STATES = 2;

	/**
	 * The feature id for the '<em><b>Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_STATE__NETWORK = 3;

	/**
	 * The number of structural features of the '<em>Network State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_STATE_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_STATE___UPDATE__CHOICE = 0;

	/**
	 * The operation id for the '<em>In Deadlock</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_STATE___IN_DEADLOCK = 1;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_STATE___UPDATE__TRANSITION = 2;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_STATE___INIT = 3;

	/**
	 * The number of operations of the '<em>Network State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_STATE_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link statemachine.interpreter.Choice <em>Choice</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.interpreter.Choice
	 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getChoice()
	 * @generated
	 */
	int CHOICE = 1;

	/**
	 * The number of structural features of the '<em>Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE___UPDATE__NETWORKSTATE = 0;

	/**
	 * The number of operations of the '<em>Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link statemachine.interpreter.impl.SynchronousChoiceImpl <em>Synchronous Choice</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.interpreter.impl.SynchronousChoiceImpl
	 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getSynchronousChoice()
	 * @generated
	 */
	int SYNCHRONOUS_CHOICE = 2;

	/**
	 * The feature id for the '<em><b>Sending</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_CHOICE__SENDING = CHOICE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Receiving</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_CHOICE__RECEIVING = CHOICE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Synchronous Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_CHOICE_FEATURE_COUNT = CHOICE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_CHOICE___UPDATE__NETWORKSTATE = CHOICE___UPDATE__NETWORKSTATE;

	/**
	 * The number of operations of the '<em>Synchronous Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONOUS_CHOICE_OPERATION_COUNT = CHOICE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link statemachine.interpreter.impl.AsynchronousChoiceImpl <em>Asynchronous Choice</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.interpreter.impl.AsynchronousChoiceImpl
	 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getAsynchronousChoice()
	 * @generated
	 */
	int ASYNCHRONOUS_CHOICE = 3;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_CHOICE__TRANSITION = CHOICE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Asynchronous Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_CHOICE_FEATURE_COUNT = CHOICE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_CHOICE___UPDATE__NETWORKSTATE = CHOICE___UPDATE__NETWORKSTATE;

	/**
	 * The number of operations of the '<em>Asynchronous Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNCHRONOUS_CHOICE_OPERATION_COUNT = CHOICE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link statemachine.interpreter.impl.StateMachineToStateMapEntryImpl <em>State Machine To State Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.interpreter.impl.StateMachineToStateMapEntryImpl
	 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getStateMachineToStateMapEntry()
	 * @generated
	 */
	int STATE_MACHINE_TO_STATE_MAP_ENTRY = 4;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TO_STATE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TO_STATE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>State Machine To State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TO_STATE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>State Machine To State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TO_STATE_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link statemachine.interpreter.impl.ChannelToIntMapEntryImpl <em>Channel To Int Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.interpreter.impl.ChannelToIntMapEntryImpl
	 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getChannelToIntMapEntry()
	 * @generated
	 */
	int CHANNEL_TO_INT_MAP_ENTRY = 5;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANNEL_TO_INT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANNEL_TO_INT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Channel To Int Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANNEL_TO_INT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Channel To Int Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANNEL_TO_INT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link statemachine.interpreter.impl.InterpreterImpl <em>Interpreter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.interpreter.impl.InterpreterImpl
	 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getInterpreter()
	 * @generated
	 */
	int INTERPRETER = 6;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPRETER__STATE = 0;

	/**
	 * The number of structural features of the '<em>Interpreter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPRETER_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPRETER___EXECUTE__NETWORK = 0;

	/**
	 * The number of operations of the '<em>Interpreter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPRETER_OPERATION_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link statemachine.interpreter.NetworkState <em>Network State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Network State</em>'.
	 * @see statemachine.interpreter.NetworkState
	 * @generated
	 */
	EClass getNetworkState();

	/**
	 * Returns the meta object for the containment reference list '{@link statemachine.interpreter.NetworkState#getAvailableTransitions <em>Available Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Available Transitions</em>'.
	 * @see statemachine.interpreter.NetworkState#getAvailableTransitions()
	 * @see #getNetworkState()
	 * @generated
	 */
	EReference getNetworkState_AvailableTransitions();

	/**
	 * Returns the meta object for the map '{@link statemachine.interpreter.NetworkState#getChannelStates <em>Channel States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Channel States</em>'.
	 * @see statemachine.interpreter.NetworkState#getChannelStates()
	 * @see #getNetworkState()
	 * @generated
	 */
	EReference getNetworkState_ChannelStates();

	/**
	 * Returns the meta object for the map '{@link statemachine.interpreter.NetworkState#getStatemachineStates <em>Statemachine States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Statemachine States</em>'.
	 * @see statemachine.interpreter.NetworkState#getStatemachineStates()
	 * @see #getNetworkState()
	 * @generated
	 */
	EReference getNetworkState_StatemachineStates();

	/**
	 * Returns the meta object for the reference '{@link statemachine.interpreter.NetworkState#getNetwork <em>Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Network</em>'.
	 * @see statemachine.interpreter.NetworkState#getNetwork()
	 * @see #getNetworkState()
	 * @generated
	 */
	EReference getNetworkState_Network();

	/**
	 * Returns the meta object for the '{@link statemachine.interpreter.NetworkState#update(statemachine.interpreter.Choice) <em>Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update</em>' operation.
	 * @see statemachine.interpreter.NetworkState#update(statemachine.interpreter.Choice)
	 * @generated
	 */
	EOperation getNetworkState__Update__Choice();

	/**
	 * Returns the meta object for the '{@link statemachine.interpreter.NetworkState#inDeadlock() <em>In Deadlock</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>In Deadlock</em>' operation.
	 * @see statemachine.interpreter.NetworkState#inDeadlock()
	 * @generated
	 */
	EOperation getNetworkState__InDeadlock();

	/**
	 * Returns the meta object for the '{@link statemachine.interpreter.NetworkState#update(statemachine.Transition) <em>Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update</em>' operation.
	 * @see statemachine.interpreter.NetworkState#update(statemachine.Transition)
	 * @generated
	 */
	EOperation getNetworkState__Update__Transition();

	/**
	 * Returns the meta object for the '{@link statemachine.interpreter.NetworkState#init() <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see statemachine.interpreter.NetworkState#init()
	 * @generated
	 */
	EOperation getNetworkState__Init();

	/**
	 * Returns the meta object for class '{@link statemachine.interpreter.Choice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Choice</em>'.
	 * @see statemachine.interpreter.Choice
	 * @generated
	 */
	EClass getChoice();

	/**
	 * Returns the meta object for the '{@link statemachine.interpreter.Choice#update(statemachine.interpreter.NetworkState) <em>Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update</em>' operation.
	 * @see statemachine.interpreter.Choice#update(statemachine.interpreter.NetworkState)
	 * @generated
	 */
	EOperation getChoice__Update__NetworkState();

	/**
	 * Returns the meta object for class '{@link statemachine.interpreter.SynchronousChoice <em>Synchronous Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Synchronous Choice</em>'.
	 * @see statemachine.interpreter.SynchronousChoice
	 * @generated
	 */
	EClass getSynchronousChoice();

	/**
	 * Returns the meta object for the reference '{@link statemachine.interpreter.SynchronousChoice#getSending <em>Sending</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sending</em>'.
	 * @see statemachine.interpreter.SynchronousChoice#getSending()
	 * @see #getSynchronousChoice()
	 * @generated
	 */
	EReference getSynchronousChoice_Sending();

	/**
	 * Returns the meta object for the reference '{@link statemachine.interpreter.SynchronousChoice#getReceiving <em>Receiving</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Receiving</em>'.
	 * @see statemachine.interpreter.SynchronousChoice#getReceiving()
	 * @see #getSynchronousChoice()
	 * @generated
	 */
	EReference getSynchronousChoice_Receiving();

	/**
	 * Returns the meta object for class '{@link statemachine.interpreter.AsynchronousChoice <em>Asynchronous Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asynchronous Choice</em>'.
	 * @see statemachine.interpreter.AsynchronousChoice
	 * @generated
	 */
	EClass getAsynchronousChoice();

	/**
	 * Returns the meta object for the reference '{@link statemachine.interpreter.AsynchronousChoice#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transition</em>'.
	 * @see statemachine.interpreter.AsynchronousChoice#getTransition()
	 * @see #getAsynchronousChoice()
	 * @generated
	 */
	EReference getAsynchronousChoice_Transition();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>State Machine To State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Machine To State Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="statemachine.StateMachine"
	 *        valueType="statemachine.State"
	 * @generated
	 */
	EClass getStateMachineToStateMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateMachineToStateMapEntry()
	 * @generated
	 */
	EReference getStateMachineToStateMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateMachineToStateMapEntry()
	 * @generated
	 */
	EReference getStateMachineToStateMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Channel To Int Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Channel To Int Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="statemachine.Channel"
	 *        valueDataType="org.eclipse.emf.ecore.EIntegerObject"
	 * @generated
	 */
	EClass getChannelToIntMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getChannelToIntMapEntry()
	 * @generated
	 */
	EReference getChannelToIntMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getChannelToIntMapEntry()
	 * @generated
	 */
	EAttribute getChannelToIntMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link statemachine.interpreter.Interpreter <em>Interpreter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interpreter</em>'.
	 * @see statemachine.interpreter.Interpreter
	 * @generated
	 */
	EClass getInterpreter();

	/**
	 * Returns the meta object for the reference '{@link statemachine.interpreter.Interpreter#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see statemachine.interpreter.Interpreter#getState()
	 * @see #getInterpreter()
	 * @generated
	 */
	EReference getInterpreter_State();

	/**
	 * Returns the meta object for the '{@link statemachine.interpreter.Interpreter#execute(statemachine.Network) <em>Execute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute</em>' operation.
	 * @see statemachine.interpreter.Interpreter#execute(statemachine.Network)
	 * @generated
	 */
	EOperation getInterpreter__Execute__Network();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	InterpreterFactory getInterpreterFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link statemachine.interpreter.impl.NetworkStateImpl <em>Network State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.interpreter.impl.NetworkStateImpl
		 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getNetworkState()
		 * @generated
		 */
		EClass NETWORK_STATE = eINSTANCE.getNetworkState();

		/**
		 * The meta object literal for the '<em><b>Available Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NETWORK_STATE__AVAILABLE_TRANSITIONS = eINSTANCE.getNetworkState_AvailableTransitions();

		/**
		 * The meta object literal for the '<em><b>Channel States</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NETWORK_STATE__CHANNEL_STATES = eINSTANCE.getNetworkState_ChannelStates();

		/**
		 * The meta object literal for the '<em><b>Statemachine States</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NETWORK_STATE__STATEMACHINE_STATES = eINSTANCE.getNetworkState_StatemachineStates();

		/**
		 * The meta object literal for the '<em><b>Network</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NETWORK_STATE__NETWORK = eINSTANCE.getNetworkState_Network();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NETWORK_STATE___UPDATE__CHOICE = eINSTANCE.getNetworkState__Update__Choice();

		/**
		 * The meta object literal for the '<em><b>In Deadlock</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NETWORK_STATE___IN_DEADLOCK = eINSTANCE.getNetworkState__InDeadlock();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NETWORK_STATE___UPDATE__TRANSITION = eINSTANCE.getNetworkState__Update__Transition();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NETWORK_STATE___INIT = eINSTANCE.getNetworkState__Init();

		/**
		 * The meta object literal for the '{@link statemachine.interpreter.Choice <em>Choice</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.interpreter.Choice
		 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getChoice()
		 * @generated
		 */
		EClass CHOICE = eINSTANCE.getChoice();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CHOICE___UPDATE__NETWORKSTATE = eINSTANCE.getChoice__Update__NetworkState();

		/**
		 * The meta object literal for the '{@link statemachine.interpreter.impl.SynchronousChoiceImpl <em>Synchronous Choice</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.interpreter.impl.SynchronousChoiceImpl
		 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getSynchronousChoice()
		 * @generated
		 */
		EClass SYNCHRONOUS_CHOICE = eINSTANCE.getSynchronousChoice();

		/**
		 * The meta object literal for the '<em><b>Sending</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNCHRONOUS_CHOICE__SENDING = eINSTANCE.getSynchronousChoice_Sending();

		/**
		 * The meta object literal for the '<em><b>Receiving</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNCHRONOUS_CHOICE__RECEIVING = eINSTANCE.getSynchronousChoice_Receiving();

		/**
		 * The meta object literal for the '{@link statemachine.interpreter.impl.AsynchronousChoiceImpl <em>Asynchronous Choice</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.interpreter.impl.AsynchronousChoiceImpl
		 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getAsynchronousChoice()
		 * @generated
		 */
		EClass ASYNCHRONOUS_CHOICE = eINSTANCE.getAsynchronousChoice();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASYNCHRONOUS_CHOICE__TRANSITION = eINSTANCE.getAsynchronousChoice_Transition();

		/**
		 * The meta object literal for the '{@link statemachine.interpreter.impl.StateMachineToStateMapEntryImpl <em>State Machine To State Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.interpreter.impl.StateMachineToStateMapEntryImpl
		 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getStateMachineToStateMapEntry()
		 * @generated
		 */
		EClass STATE_MACHINE_TO_STATE_MAP_ENTRY = eINSTANCE.getStateMachineToStateMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE_TO_STATE_MAP_ENTRY__KEY = eINSTANCE.getStateMachineToStateMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE_TO_STATE_MAP_ENTRY__VALUE = eINSTANCE.getStateMachineToStateMapEntry_Value();

		/**
		 * The meta object literal for the '{@link statemachine.interpreter.impl.ChannelToIntMapEntryImpl <em>Channel To Int Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.interpreter.impl.ChannelToIntMapEntryImpl
		 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getChannelToIntMapEntry()
		 * @generated
		 */
		EClass CHANNEL_TO_INT_MAP_ENTRY = eINSTANCE.getChannelToIntMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANNEL_TO_INT_MAP_ENTRY__KEY = eINSTANCE.getChannelToIntMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHANNEL_TO_INT_MAP_ENTRY__VALUE = eINSTANCE.getChannelToIntMapEntry_Value();

		/**
		 * The meta object literal for the '{@link statemachine.interpreter.impl.InterpreterImpl <em>Interpreter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.interpreter.impl.InterpreterImpl
		 * @see statemachine.interpreter.impl.InterpreterPackageImpl#getInterpreter()
		 * @generated
		 */
		EClass INTERPRETER = eINSTANCE.getInterpreter();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERPRETER__STATE = eINSTANCE.getInterpreter_State();

		/**
		 * The meta object literal for the '<em><b>Execute</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INTERPRETER___EXECUTE__NETWORK = eINSTANCE.getInterpreter__Execute__Network();

	}

} //InterpreterPackage
