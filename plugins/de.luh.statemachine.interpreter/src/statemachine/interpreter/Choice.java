/**
 */
package statemachine.interpreter;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Choice</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see statemachine.interpreter.InterpreterPackage#getChoice()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Choice extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void update(NetworkState state);

} // Choice
