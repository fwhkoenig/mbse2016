/**
 */
package statemachine.interpreter;

import org.eclipse.emf.ecore.EObject;

import statemachine.Network;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interpreter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.interpreter.Interpreter#getState <em>State</em>}</li>
 * </ul>
 *
 * @see statemachine.interpreter.InterpreterPackage#getInterpreter()
 * @model
 * @generated
 */
public interface Interpreter extends EObject {
	/**
	 * Returns the value of the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' reference.
	 * @see #setState(NetworkState)
	 * @see statemachine.interpreter.InterpreterPackage#getInterpreter_State()
	 * @model
	 * @generated
	 */
	NetworkState getState();

	/**
	 * Sets the value of the '{@link statemachine.interpreter.Interpreter#getState <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' reference.
	 * @see #getState()
	 * @generated
	 */
	void setState(NetworkState value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void execute(Network network);

} // Interpreter
