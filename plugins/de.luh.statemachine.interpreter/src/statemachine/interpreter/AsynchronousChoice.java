/**
 */
package statemachine.interpreter;

import statemachine.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asynchronous Choice</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.interpreter.AsynchronousChoice#getTransition <em>Transition</em>}</li>
 * </ul>
 *
 * @see statemachine.interpreter.InterpreterPackage#getAsynchronousChoice()
 * @model
 * @generated
 */
public interface AsynchronousChoice extends Choice {
	/**
	 * Returns the value of the '<em><b>Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' reference.
	 * @see #setTransition(Transition)
	 * @see statemachine.interpreter.InterpreterPackage#getAsynchronousChoice_Transition()
	 * @model
	 * @generated
	 */
	Transition getTransition();

	/**
	 * Sets the value of the '{@link statemachine.interpreter.AsynchronousChoice#getTransition <em>Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition</em>' reference.
	 * @see #getTransition()
	 * @generated
	 */
	void setTransition(Transition value);

} // AsynchronousChoice
