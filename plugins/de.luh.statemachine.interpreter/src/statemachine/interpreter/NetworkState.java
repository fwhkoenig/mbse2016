/**
 */
package statemachine.interpreter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import statemachine.Channel;
import statemachine.Network;
import statemachine.State;
import statemachine.StateMachine;
import statemachine.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Network State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.interpreter.NetworkState#getAvailableTransitions <em>Available Transitions</em>}</li>
 *   <li>{@link statemachine.interpreter.NetworkState#getChannelStates <em>Channel States</em>}</li>
 *   <li>{@link statemachine.interpreter.NetworkState#getStatemachineStates <em>Statemachine States</em>}</li>
 *   <li>{@link statemachine.interpreter.NetworkState#getNetwork <em>Network</em>}</li>
 * </ul>
 *
 * @see statemachine.interpreter.InterpreterPackage#getNetworkState()
 * @model
 * @generated
 */
public interface NetworkState extends EObject {
	/**
	 * Returns the value of the '<em><b>Available Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link statemachine.interpreter.Choice}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Available Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Available Transitions</em>' containment reference list.
	 * @see statemachine.interpreter.InterpreterPackage#getNetworkState_AvailableTransitions()
	 * @model containment="true" derived="true"
	 * @generated
	 */
	EList<Choice> getAvailableTransitions();

	/**
	 * Returns the value of the '<em><b>Channel States</b></em>' map.
	 * The key is of type {@link statemachine.Channel},
	 * and the value is of type {@link java.lang.Integer},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Channel States</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Channel States</em>' map.
	 * @see statemachine.interpreter.InterpreterPackage#getNetworkState_ChannelStates()
	 * @model mapType="statemachine.interpreter.ChannelToIntMapEntry<statemachine.Channel, org.eclipse.emf.ecore.EIntegerObject>"
	 * @generated
	 */
	EMap<Channel, Integer> getChannelStates();

	/**
	 * Returns the value of the '<em><b>Statemachine States</b></em>' map.
	 * The key is of type {@link statemachine.StateMachine},
	 * and the value is of type {@link statemachine.State},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statemachine States</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statemachine States</em>' map.
	 * @see statemachine.interpreter.InterpreterPackage#getNetworkState_StatemachineStates()
	 * @model mapType="statemachine.interpreter.StateMachineToStateMapEntry<statemachine.StateMachine, statemachine.State>"
	 * @generated
	 */
	EMap<StateMachine, State> getStatemachineStates();

	/**
	 * Returns the value of the '<em><b>Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Network</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Network</em>' reference.
	 * @see #setNetwork(Network)
	 * @see statemachine.interpreter.InterpreterPackage#getNetworkState_Network()
	 * @model
	 * @generated
	 */
	Network getNetwork();

	/**
	 * Sets the value of the '{@link statemachine.interpreter.NetworkState#getNetwork <em>Network</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Network</em>' reference.
	 * @see #getNetwork()
	 * @generated
	 */
	void setNetwork(Network value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void update(Choice choice);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean inDeadlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void update(Transition transition);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init();

} // NetworkState
