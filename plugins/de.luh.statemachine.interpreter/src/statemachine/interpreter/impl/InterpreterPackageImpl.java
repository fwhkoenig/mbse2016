/**
 */
package statemachine.interpreter.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import statemachine.StatemachinePackage;

import statemachine.interpreter.AsynchronousChoice;
import statemachine.interpreter.Choice;
import statemachine.interpreter.Interpreter;
import statemachine.interpreter.InterpreterFactory;
import statemachine.interpreter.InterpreterPackage;
import statemachine.interpreter.NetworkState;
import statemachine.interpreter.SynchronousChoice;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InterpreterPackageImpl extends EPackageImpl implements InterpreterPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass networkStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass choiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass synchronousChoiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass asynchronousChoiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateMachineToStateMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass channelToIntMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interpreterEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see statemachine.interpreter.InterpreterPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private InterpreterPackageImpl() {
		super(eNS_URI, InterpreterFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link InterpreterPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static InterpreterPackage init() {
		if (isInited) return (InterpreterPackage)EPackage.Registry.INSTANCE.getEPackage(InterpreterPackage.eNS_URI);

		// Obtain or create and register package
		InterpreterPackageImpl theInterpreterPackage = (InterpreterPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof InterpreterPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new InterpreterPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		StatemachinePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theInterpreterPackage.createPackageContents();

		// Initialize created meta-data
		theInterpreterPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theInterpreterPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(InterpreterPackage.eNS_URI, theInterpreterPackage);
		return theInterpreterPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNetworkState() {
		return networkStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNetworkState_AvailableTransitions() {
		return (EReference)networkStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNetworkState_ChannelStates() {
		return (EReference)networkStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNetworkState_StatemachineStates() {
		return (EReference)networkStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNetworkState_Network() {
		return (EReference)networkStateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNetworkState__Update__Choice() {
		return networkStateEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNetworkState__InDeadlock() {
		return networkStateEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNetworkState__Update__Transition() {
		return networkStateEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNetworkState__Init() {
		return networkStateEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChoice() {
		return choiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getChoice__Update__NetworkState() {
		return choiceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSynchronousChoice() {
		return synchronousChoiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSynchronousChoice_Sending() {
		return (EReference)synchronousChoiceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSynchronousChoice_Receiving() {
		return (EReference)synchronousChoiceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAsynchronousChoice() {
		return asynchronousChoiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAsynchronousChoice_Transition() {
		return (EReference)asynchronousChoiceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStateMachineToStateMapEntry() {
		return stateMachineToStateMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateMachineToStateMapEntry_Key() {
		return (EReference)stateMachineToStateMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateMachineToStateMapEntry_Value() {
		return (EReference)stateMachineToStateMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChannelToIntMapEntry() {
		return channelToIntMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChannelToIntMapEntry_Key() {
		return (EReference)channelToIntMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChannelToIntMapEntry_Value() {
		return (EAttribute)channelToIntMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterpreter() {
		return interpreterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterpreter_State() {
		return (EReference)interpreterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getInterpreter__Execute__Network() {
		return interpreterEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterFactory getInterpreterFactory() {
		return (InterpreterFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		networkStateEClass = createEClass(NETWORK_STATE);
		createEReference(networkStateEClass, NETWORK_STATE__AVAILABLE_TRANSITIONS);
		createEReference(networkStateEClass, NETWORK_STATE__CHANNEL_STATES);
		createEReference(networkStateEClass, NETWORK_STATE__STATEMACHINE_STATES);
		createEReference(networkStateEClass, NETWORK_STATE__NETWORK);
		createEOperation(networkStateEClass, NETWORK_STATE___UPDATE__CHOICE);
		createEOperation(networkStateEClass, NETWORK_STATE___IN_DEADLOCK);
		createEOperation(networkStateEClass, NETWORK_STATE___UPDATE__TRANSITION);
		createEOperation(networkStateEClass, NETWORK_STATE___INIT);

		choiceEClass = createEClass(CHOICE);
		createEOperation(choiceEClass, CHOICE___UPDATE__NETWORKSTATE);

		synchronousChoiceEClass = createEClass(SYNCHRONOUS_CHOICE);
		createEReference(synchronousChoiceEClass, SYNCHRONOUS_CHOICE__SENDING);
		createEReference(synchronousChoiceEClass, SYNCHRONOUS_CHOICE__RECEIVING);

		asynchronousChoiceEClass = createEClass(ASYNCHRONOUS_CHOICE);
		createEReference(asynchronousChoiceEClass, ASYNCHRONOUS_CHOICE__TRANSITION);

		stateMachineToStateMapEntryEClass = createEClass(STATE_MACHINE_TO_STATE_MAP_ENTRY);
		createEReference(stateMachineToStateMapEntryEClass, STATE_MACHINE_TO_STATE_MAP_ENTRY__KEY);
		createEReference(stateMachineToStateMapEntryEClass, STATE_MACHINE_TO_STATE_MAP_ENTRY__VALUE);

		channelToIntMapEntryEClass = createEClass(CHANNEL_TO_INT_MAP_ENTRY);
		createEReference(channelToIntMapEntryEClass, CHANNEL_TO_INT_MAP_ENTRY__KEY);
		createEAttribute(channelToIntMapEntryEClass, CHANNEL_TO_INT_MAP_ENTRY__VALUE);

		interpreterEClass = createEClass(INTERPRETER);
		createEReference(interpreterEClass, INTERPRETER__STATE);
		createEOperation(interpreterEClass, INTERPRETER___EXECUTE__NETWORK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StatemachinePackage theStatemachinePackage = (StatemachinePackage)EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		synchronousChoiceEClass.getESuperTypes().add(this.getChoice());
		asynchronousChoiceEClass.getESuperTypes().add(this.getChoice());

		// Initialize classes, features, and operations; add parameters
		initEClass(networkStateEClass, NetworkState.class, "NetworkState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNetworkState_AvailableTransitions(), this.getChoice(), null, "availableTransitions", null, 0, -1, NetworkState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getNetworkState_ChannelStates(), this.getChannelToIntMapEntry(), null, "channelStates", null, 0, -1, NetworkState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNetworkState_StatemachineStates(), this.getStateMachineToStateMapEntry(), null, "statemachineStates", null, 0, -1, NetworkState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNetworkState_Network(), theStatemachinePackage.getNetwork(), null, "network", null, 0, 1, NetworkState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getNetworkState__Update__Choice(), null, "update", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getChoice(), "choice", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getNetworkState__InDeadlock(), ecorePackage.getEBoolean(), "inDeadlock", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getNetworkState__Update__Transition(), null, "update", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theStatemachinePackage.getTransition(), "transition", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getNetworkState__Init(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(choiceEClass, Choice.class, "Choice", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getChoice__Update__NetworkState(), null, "update", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNetworkState(), "state", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(synchronousChoiceEClass, SynchronousChoice.class, "SynchronousChoice", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSynchronousChoice_Sending(), theStatemachinePackage.getTransition(), null, "sending", null, 0, 1, SynchronousChoice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSynchronousChoice_Receiving(), theStatemachinePackage.getTransition(), null, "receiving", null, 0, 1, SynchronousChoice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(asynchronousChoiceEClass, AsynchronousChoice.class, "AsynchronousChoice", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAsynchronousChoice_Transition(), theStatemachinePackage.getTransition(), null, "transition", null, 0, 1, AsynchronousChoice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stateMachineToStateMapEntryEClass, Map.Entry.class, "StateMachineToStateMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStateMachineToStateMapEntry_Key(), theStatemachinePackage.getStateMachine(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateMachineToStateMapEntry_Value(), theStatemachinePackage.getState(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(channelToIntMapEntryEClass, Map.Entry.class, "ChannelToIntMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getChannelToIntMapEntry_Key(), theStatemachinePackage.getChannel(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChannelToIntMapEntry_Value(), ecorePackage.getEIntegerObject(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interpreterEClass, Interpreter.class, "Interpreter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInterpreter_State(), this.getNetworkState(), null, "state", null, 0, 1, Interpreter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getInterpreter__Execute__Network(), null, "execute", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theStatemachinePackage.getNetwork(), "network", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //InterpreterPackageImpl
