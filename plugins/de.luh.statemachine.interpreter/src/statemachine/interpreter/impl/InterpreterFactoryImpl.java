/**
 */
package statemachine.interpreter.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import statemachine.Channel;
import statemachine.State;
import statemachine.StateMachine;

import statemachine.interpreter.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InterpreterFactoryImpl extends EFactoryImpl implements InterpreterFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static InterpreterFactory init() {
		try {
			InterpreterFactory theInterpreterFactory = (InterpreterFactory)EPackage.Registry.INSTANCE.getEFactory(InterpreterPackage.eNS_URI);
			if (theInterpreterFactory != null) {
				return theInterpreterFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new InterpreterFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case InterpreterPackage.NETWORK_STATE: return createNetworkState();
			case InterpreterPackage.SYNCHRONOUS_CHOICE: return createSynchronousChoice();
			case InterpreterPackage.ASYNCHRONOUS_CHOICE: return createAsynchronousChoice();
			case InterpreterPackage.STATE_MACHINE_TO_STATE_MAP_ENTRY: return (EObject)createStateMachineToStateMapEntry();
			case InterpreterPackage.CHANNEL_TO_INT_MAP_ENTRY: return (EObject)createChannelToIntMapEntry();
			case InterpreterPackage.INTERPRETER: return createInterpreter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NetworkState createNetworkState() {
		NetworkStateImpl networkState = new NetworkStateImpl();
		return networkState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SynchronousChoice createSynchronousChoice() {
		SynchronousChoiceImpl synchronousChoice = new SynchronousChoiceImpl();
		return synchronousChoice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousChoice createAsynchronousChoice() {
		AsynchronousChoiceImpl asynchronousChoice = new AsynchronousChoiceImpl();
		return asynchronousChoice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<StateMachine, State> createStateMachineToStateMapEntry() {
		StateMachineToStateMapEntryImpl stateMachineToStateMapEntry = new StateMachineToStateMapEntryImpl();
		return stateMachineToStateMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Channel, Integer> createChannelToIntMapEntry() {
		ChannelToIntMapEntryImpl channelToIntMapEntry = new ChannelToIntMapEntryImpl();
		return channelToIntMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interpreter createInterpreter() {
		InterpreterImpl interpreter = new InterpreterImpl();
		return interpreter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterpreterPackage getInterpreterPackage() {
		return (InterpreterPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static InterpreterPackage getPackage() {
		return InterpreterPackage.eINSTANCE;
	}

} //InterpreterFactoryImpl
