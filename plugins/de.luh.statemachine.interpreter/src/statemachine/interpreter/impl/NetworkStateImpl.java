/**
 */
package statemachine.interpreter.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import statemachine.Channel;
import statemachine.Network;
import statemachine.State;
import statemachine.StateMachine;
import statemachine.Transition;
import statemachine.interpreter.AsynchronousChoice;
import statemachine.interpreter.Choice;
import statemachine.interpreter.InterpreterFactory;
import statemachine.interpreter.InterpreterPackage;
import statemachine.interpreter.NetworkState;
import statemachine.interpreter.SynchronousChoice;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Network State</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link statemachine.interpreter.impl.NetworkStateImpl#getAvailableTransitions <em>Available Transitions</em>}</li>
 *   <li>{@link statemachine.interpreter.impl.NetworkStateImpl#getChannelStates <em>Channel States</em>}</li>
 *   <li>{@link statemachine.interpreter.impl.NetworkStateImpl#getStatemachineStates <em>Statemachine States</em>}</li>
 *   <li>{@link statemachine.interpreter.impl.NetworkStateImpl#getNetwork <em>Network</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NetworkStateImpl extends MinimalEObjectImpl.Container implements NetworkState {
	/**
	 * The cached value of the '{@link #getAvailableTransitions() <em>Available Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailableTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Choice> availableTransitions;

	/**
	 * The cached value of the '{@link #getChannelStates()
	 * <em>Channel States</em>}' map. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getChannelStates()
	 * @generated
	 * @ordered
	 */
	protected EMap<Channel, Integer> channelStates;

	/**
	 * The cached value of the '{@link #getStatemachineStates() <em>Statemachine States</em>}' map.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getStatemachineStates()
	 * @generated
	 * @ordered
	 */
	protected EMap<StateMachine, State> statemachineStates;

	/**
	 * The cached value of the '{@link #getNetwork() <em>Network</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNetwork()
	 * @generated
	 * @ordered
	 */
	protected Network network;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected NetworkStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.NETWORK_STATE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Choice> getAvailableTransitions() {
		if (availableTransitions == null) {
			availableTransitions = new EObjectContainmentEList<Choice>(Choice.class, this, InterpreterPackage.NETWORK_STATE__AVAILABLE_TRANSITIONS);
		}
		return availableTransitions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Channel, Integer> getChannelStates() {
		if (channelStates == null) {
			channelStates = new EcoreEMap<Channel,Integer>(InterpreterPackage.Literals.CHANNEL_TO_INT_MAP_ENTRY, ChannelToIntMapEntryImpl.class, this, InterpreterPackage.NETWORK_STATE__CHANNEL_STATES);
		}
		return channelStates;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<StateMachine, State> getStatemachineStates() {
		if (statemachineStates == null) {
			statemachineStates = new EcoreEMap<StateMachine,State>(InterpreterPackage.Literals.STATE_MACHINE_TO_STATE_MAP_ENTRY, StateMachineToStateMapEntryImpl.class, this, InterpreterPackage.NETWORK_STATE__STATEMACHINE_STATES);
		}
		return statemachineStates;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Network getNetwork() {
		if (network != null && network.eIsProxy()) {
			InternalEObject oldNetwork = (InternalEObject)network;
			network = (Network)eResolveProxy(oldNetwork);
			if (network != oldNetwork) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.NETWORK_STATE__NETWORK, oldNetwork, network));
			}
		}
		return network;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Network basicGetNetwork() {
		return network;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setNetwork(Network newNetwork) {
		Network oldNetwork = network;
		network = newNetwork;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.NETWORK_STATE__NETWORK, oldNetwork, network));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void update(Choice choice) {
		choice.update(this);
		updateAvailableChoices();
	}

	private void updateAvailableChoices() {
		getAvailableTransitions().clear();
		Map<Channel, Set<Transition>> sendMap = new HashMap<>();
		Map<Channel, Set<Transition>> receiveMap = new HashMap<>();
		for (StateMachine machine : statemachineStates.keySet()) {
			State currentState = statemachineStates.get(machine);
			for (Transition t : currentState.getOutgoing()) {
				if (t.getChannel().isSynchronous()) {
					Map<Channel, Set<Transition>> map = t.isSending() ? sendMap : receiveMap;
					map.computeIfAbsent(t.getChannel(), c -> new HashSet<>()).add(t);
				} else {
					if(!t.isSending() && getChannelStates().get(t.getChannel())<=0)
						continue;
					AsynchronousChoice choice = InterpreterFactory.eINSTANCE.createAsynchronousChoice();
					choice.setTransition(t);
					getAvailableTransitions().add(choice);
				}

			}
		}
		for (Channel channel : sendMap.keySet()) {
			for (Transition sending : sendMap.get(channel)) {
				for (Transition receiving : receiveMap.computeIfAbsent(channel, c -> Collections.emptySet())) {
					SynchronousChoice choice = InterpreterFactory.eINSTANCE.createSynchronousChoice();
					choice.setSending(sending);
					choice.setReceiving(receiving);
					getAvailableTransitions().add(choice);
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean inDeadlock() {
		return getAvailableTransitions().isEmpty();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void update(Transition transition) {
		getStatemachineStates().put((StateMachine) transition.eContainer(), transition.getTarget());
		final Channel channel = transition.getChannel();
		if (!channel.isSynchronous()) {
			int delta = transition.isSending() ? 1 : -1;
			getChannelStates().put(channel, getChannelStates().get(channel) + delta);
		}
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		for (Entry<Channel, Integer> entry : getChannelStates()) {
			b.append(entry.getKey().getName()+":"+ entry.getValue() + "\n");
		}
		for (Entry<StateMachine, State> entry : getStatemachineStates()) {
			b.append(entry.getKey().getName() + ": "+ entry.getValue().getName() + "\n");
		}
		return b.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void init() {
		for (Channel c : getNetwork().getChannels()) {
			getChannelStates().put(c, 0);
		}
		for (StateMachine machine : getNetwork().getStateMachines()) {
			getStatemachineStates().put(machine, machine.getInitialState());
		}
		updateAvailableChoices();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InterpreterPackage.NETWORK_STATE__AVAILABLE_TRANSITIONS:
				return ((InternalEList<?>)getAvailableTransitions()).basicRemove(otherEnd, msgs);
			case InterpreterPackage.NETWORK_STATE__CHANNEL_STATES:
				return ((InternalEList<?>)getChannelStates()).basicRemove(otherEnd, msgs);
			case InterpreterPackage.NETWORK_STATE__STATEMACHINE_STATES:
				return ((InternalEList<?>)getStatemachineStates()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.NETWORK_STATE__AVAILABLE_TRANSITIONS:
				return getAvailableTransitions();
			case InterpreterPackage.NETWORK_STATE__CHANNEL_STATES:
				if (coreType) return getChannelStates();
				else return getChannelStates().map();
			case InterpreterPackage.NETWORK_STATE__STATEMACHINE_STATES:
				if (coreType) return getStatemachineStates();
				else return getStatemachineStates().map();
			case InterpreterPackage.NETWORK_STATE__NETWORK:
				if (resolve) return getNetwork();
				return basicGetNetwork();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.NETWORK_STATE__AVAILABLE_TRANSITIONS:
				getAvailableTransitions().clear();
				getAvailableTransitions().addAll((Collection<? extends Choice>)newValue);
				return;
			case InterpreterPackage.NETWORK_STATE__CHANNEL_STATES:
				((EStructuralFeature.Setting)getChannelStates()).set(newValue);
				return;
			case InterpreterPackage.NETWORK_STATE__STATEMACHINE_STATES:
				((EStructuralFeature.Setting)getStatemachineStates()).set(newValue);
				return;
			case InterpreterPackage.NETWORK_STATE__NETWORK:
				setNetwork((Network)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.NETWORK_STATE__AVAILABLE_TRANSITIONS:
				getAvailableTransitions().clear();
				return;
			case InterpreterPackage.NETWORK_STATE__CHANNEL_STATES:
				getChannelStates().clear();
				return;
			case InterpreterPackage.NETWORK_STATE__STATEMACHINE_STATES:
				getStatemachineStates().clear();
				return;
			case InterpreterPackage.NETWORK_STATE__NETWORK:
				setNetwork((Network)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.NETWORK_STATE__AVAILABLE_TRANSITIONS:
				return availableTransitions != null && !availableTransitions.isEmpty();
			case InterpreterPackage.NETWORK_STATE__CHANNEL_STATES:
				return channelStates != null && !channelStates.isEmpty();
			case InterpreterPackage.NETWORK_STATE__STATEMACHINE_STATES:
				return statemachineStates != null && !statemachineStates.isEmpty();
			case InterpreterPackage.NETWORK_STATE__NETWORK:
				return network != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case InterpreterPackage.NETWORK_STATE___UPDATE__CHOICE:
				update((Choice)arguments.get(0));
				return null;
			case InterpreterPackage.NETWORK_STATE___IN_DEADLOCK:
				return inDeadlock();
			case InterpreterPackage.NETWORK_STATE___UPDATE__TRANSITION:
				update((Transition)arguments.get(0));
				return null;
			case InterpreterPackage.NETWORK_STATE___INIT:
				init();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} // NetworkStateImpl
