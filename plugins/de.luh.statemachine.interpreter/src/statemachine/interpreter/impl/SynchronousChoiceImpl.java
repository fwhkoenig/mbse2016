/**
 */
package statemachine.interpreter.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import statemachine.Transition;

import statemachine.interpreter.InterpreterPackage;
import statemachine.interpreter.NetworkState;
import statemachine.interpreter.SynchronousChoice;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Synchronous Choice</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link statemachine.interpreter.impl.SynchronousChoiceImpl#getSending <em>Sending</em>}</li>
 *   <li>{@link statemachine.interpreter.impl.SynchronousChoiceImpl#getReceiving <em>Receiving</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SynchronousChoiceImpl extends MinimalEObjectImpl.Container implements SynchronousChoice {
	/**
	 * The cached value of the '{@link #getSending() <em>Sending</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSending()
	 * @generated
	 * @ordered
	 */
	protected Transition sending;

	/**
	 * The cached value of the '{@link #getReceiving() <em>Receiving</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getReceiving()
	 * @generated
	 * @ordered
	 */
	protected Transition receiving;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected SynchronousChoiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.SYNCHRONOUS_CHOICE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Transition getSending() {
		if (sending != null && sending.eIsProxy()) {
			InternalEObject oldSending = (InternalEObject)sending;
			sending = (Transition)eResolveProxy(oldSending);
			if (sending != oldSending) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.SYNCHRONOUS_CHOICE__SENDING, oldSending, sending));
			}
		}
		return sending;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Transition basicGetSending() {
		return sending;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSending(Transition newSending) {
		Transition oldSending = sending;
		sending = newSending;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.SYNCHRONOUS_CHOICE__SENDING, oldSending, sending));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Transition getReceiving() {
		if (receiving != null && receiving.eIsProxy()) {
			InternalEObject oldReceiving = (InternalEObject)receiving;
			receiving = (Transition)eResolveProxy(oldReceiving);
			if (receiving != oldReceiving) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.SYNCHRONOUS_CHOICE__RECEIVING, oldReceiving, receiving));
			}
		}
		return receiving;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Transition basicGetReceiving() {
		return receiving;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceiving(Transition newReceiving) {
		Transition oldReceiving = receiving;
		receiving = newReceiving;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.SYNCHRONOUS_CHOICE__RECEIVING, oldReceiving, receiving));
	}

	/**
	 * <!-- begin-user-doc --> Progresses two statemachines that send/receive
	 * over a common channel. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void update(NetworkState state) {
		state.update(getSending());
		state.update(getReceiving());
	}

	@Override
	public String toString() {
		return "("+sending+", "+receiving+")";
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.SYNCHRONOUS_CHOICE__SENDING:
				if (resolve) return getSending();
				return basicGetSending();
			case InterpreterPackage.SYNCHRONOUS_CHOICE__RECEIVING:
				if (resolve) return getReceiving();
				return basicGetReceiving();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.SYNCHRONOUS_CHOICE__SENDING:
				setSending((Transition)newValue);
				return;
			case InterpreterPackage.SYNCHRONOUS_CHOICE__RECEIVING:
				setReceiving((Transition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.SYNCHRONOUS_CHOICE__SENDING:
				setSending((Transition)null);
				return;
			case InterpreterPackage.SYNCHRONOUS_CHOICE__RECEIVING:
				setReceiving((Transition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.SYNCHRONOUS_CHOICE__SENDING:
				return sending != null;
			case InterpreterPackage.SYNCHRONOUS_CHOICE__RECEIVING:
				return receiving != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case InterpreterPackage.SYNCHRONOUS_CHOICE___UPDATE__NETWORKSTATE:
				update((NetworkState)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} // SynchronousChoiceImpl
