/**
 */
package statemachine.interpreter.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Map.Entry;
import java.util.Random;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import statemachine.Channel;
import statemachine.Network;
import statemachine.State;
import statemachine.StateMachine;
import statemachine.interpreter.Choice;
import statemachine.interpreter.Interpreter;
import statemachine.interpreter.InterpreterFactory;
import statemachine.interpreter.InterpreterPackage;
import statemachine.interpreter.NetworkState;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interpreter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link statemachine.interpreter.impl.InterpreterImpl#getState <em>State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InterpreterImpl extends MinimalEObjectImpl.Container implements Interpreter {
	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected NetworkState state;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterpreterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InterpreterPackage.Literals.INTERPRETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NetworkState getState() {
		if (state != null && state.eIsProxy()) {
			InternalEObject oldState = (InternalEObject)state;
			state = (NetworkState)eResolveProxy(oldState);
			if (state != oldState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InterpreterPackage.INTERPRETER__STATE, oldState, state));
			}
		}
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NetworkState basicGetState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(NetworkState newState) {
		NetworkState oldState = state;
		state = newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InterpreterPackage.INTERPRETER__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void execute(Network network) {
		state = InterpreterFactory.eINSTANCE.createNetworkState();
		state.init();
		printState(state);
		Random r = new Random();
		while(!state.inDeadlock()) {
			Choice c = state.getAvailableTransitions().get(r.nextInt(state.getAvailableTransitions().size()));
			printChoice(c);
			state.update(c);
			printState(state);
		}
		System.out.println("Reached Deadlock");
	}

	private void printState(NetworkState state) {
		for(Entry<Channel,Integer> entry: state.getChannelStates()) {
			System.out.println("Channel "+entry.getKey().getName()+" buffer size: "+entry.getValue());
		}
		for(Entry<StateMachine, State>entry:state.getStatemachineStates()) {
			System.out.println("Statemachine "+entry.getKey().getName()+" is in state "+entry.getValue().getName());
		}
	}

	private void printChoice(Choice c) {
		System.out.println("Choose transition "+c);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InterpreterPackage.INTERPRETER__STATE:
				if (resolve) return getState();
				return basicGetState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InterpreterPackage.INTERPRETER__STATE:
				setState((NetworkState)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InterpreterPackage.INTERPRETER__STATE:
				setState((NetworkState)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InterpreterPackage.INTERPRETER__STATE:
				return state != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case InterpreterPackage.INTERPRETER___EXECUTE__NETWORK:
				execute((Network)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //InterpreterImpl
