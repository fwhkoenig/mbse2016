/**
 */
package statemachine.interpreter;

import statemachine.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synchronous Choice</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.interpreter.SynchronousChoice#getSending <em>Sending</em>}</li>
 *   <li>{@link statemachine.interpreter.SynchronousChoice#getReceiving <em>Receiving</em>}</li>
 * </ul>
 *
 * @see statemachine.interpreter.InterpreterPackage#getSynchronousChoice()
 * @model
 * @generated
 */
public interface SynchronousChoice extends Choice {
	/**
	 * Returns the value of the '<em><b>Sending</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sending</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sending</em>' reference.
	 * @see #setSending(Transition)
	 * @see statemachine.interpreter.InterpreterPackage#getSynchronousChoice_Sending()
	 * @model
	 * @generated
	 */
	Transition getSending();

	/**
	 * Sets the value of the '{@link statemachine.interpreter.SynchronousChoice#getSending <em>Sending</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sending</em>' reference.
	 * @see #getSending()
	 * @generated
	 */
	void setSending(Transition value);

	/**
	 * Returns the value of the '<em><b>Receiving</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receiving</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiving</em>' reference.
	 * @see #setReceiving(Transition)
	 * @see statemachine.interpreter.InterpreterPackage#getSynchronousChoice_Receiving()
	 * @model
	 * @generated
	 */
	Transition getReceiving();

	/**
	 * Sets the value of the '{@link statemachine.interpreter.SynchronousChoice#getReceiving <em>Receiving</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Receiving</em>' reference.
	 * @see #getReceiving()
	 * @generated
	 */
	void setReceiving(Transition value);

} // SynchronousChoice
