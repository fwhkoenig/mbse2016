/**
 */
package statemachine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.State#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link statemachine.State#getOutgoing <em>Outgoing</em>}</li>
 * </ul>
 *
 * @see statemachine.StatemachinePackage#getState()
 * @model
 * @generated
 */
public interface State extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Incoming</b></em>' reference list.
	 * The list contents are of type {@link statemachine.Transition}.
	 * It is bidirectional and its opposite is '{@link statemachine.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming</em>' reference list.
	 * @see statemachine.StatemachinePackage#getState_Incoming()
	 * @see statemachine.Transition#getTarget
	 * @model opposite="target" resolveProxies="false" transient="true" derived="true"
	 * @generated
	 */
	EList<Transition> getIncoming();

	/**
	 * Returns the value of the '<em><b>Outgoing</b></em>' reference list.
	 * The list contents are of type {@link statemachine.Transition}.
	 * It is bidirectional and its opposite is '{@link statemachine.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing</em>' reference list.
	 * @see statemachine.StatemachinePackage#getState_Outgoing()
	 * @see statemachine.Transition#getSource
	 * @model opposite="source" resolveProxies="false" transient="true" derived="true"
	 * @generated
	 */
	EList<Transition> getOutgoing();

} // State
