# Xtext and Sirius editor for State Machine Networks #

We created two simple editors for modelling of state machine networks. A textural editor with Xtext and a graphical editor using Sirius.

![Sirius Editor.PNG](https://bitbucket.org/repo/7k8X7A/images/3769789509-Sirius%20Editor.PNG)

### Metamodel for State Machine Networks ###

![statemachine.jpg](https://bitbucket.org/repo/7k8X7A/images/773033933-statemachine.jpg)

### What is this repository for? ###

* Lecture: Model-based Software Engineering at Leibniz University of Hanover.
* Miniprojects

### Setup ###

1. Install Eclipse EMF
2. Download Xtext from Eclipse Marketplace.
3. Download Sirius from Updatesites.
4. Copy all projects under the plugins directory of this repository into your workspace.
5. Start an Eclipse Application with these plugins.

### How to use the Editor ###

1. Create a new project
2. Add Xtext nature to the project
3. Create a file with extension ‘.smn’
4. The file is opened with the Xtext editor
5. Specify your Network
6. Create a new representation file
7. Create a network diagram in that file
8. View and edit you Network

### Team ###

* Florian König
* Nils Glade
* Linh Phan